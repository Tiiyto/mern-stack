import { Avatar } from '@material-ui/core'
import { useContext } from 'react'
import { GoogleLogout } from 'react-google-login'
import { useHistory } from 'react-router-dom'
import { UserContext } from '../../Context'
import { logout } from '../../actions/auth'

const GoogleProfile = () => {
	const { auth, dispatch } = useContext(UserContext)
	const { user } = auth
	let history = useHistory()

	/**
	 *
	 * @param {Event} e
	 */
	const failure = (e) => {
		console.log(e)
	}

	return (
		<>
			<div style={{ justifyContent: 'center', display: 'flex' }}>
				<Avatar alt={user.name} src={user.imageUrl} style={{ width: '6rem', height: '6rem' }} />
			</div>
			<div className="mb-1">{user.name}</div>
			<div>
				<GoogleLogout
					clientId={process.env.REACT_APP_CLIENT_ID}
					buttonText="Logout"
					onLogoutSuccess={() => logout(dispatch, history)}
					onFailure={failure}
				/>
			</div>
		</>
	)
}

export default GoogleProfile

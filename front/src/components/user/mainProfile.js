import { Avatar, Button, Typography } from '@material-ui/core'
import { ExitToApp, Edit, ShoppingCart } from '@material-ui/icons'
import { useContext, useEffect } from 'react'
import { useHistory } from 'react-router-dom'
import { UserContext } from '../../Context'
import { logout, refreshUser } from '../../actions/auth'
const MainProfile = () => {
	const { auth, dispatch } = useContext(UserContext)
	const { user } = auth
	let history = useHistory()

	useEffect(() => {
		refreshUser(dispatch, history)
	}, [])

	return (
		<>
			<div style={{ justifyContent: 'center', display: 'flex' }}>
				<Avatar alt={user.username} src={user.imageUrl} style={{ width: '6rem', height: '6rem' }} />
			</div>
			<Typography variant="h5" component="div" gutterBottom>
				{user.username}
			</Typography>
			<div className="mb-1">
				<Button
					startIcon={<ShoppingCart />}
					variant="contained"
					color="primary"
					onClick={() => history.push('/profile/orders')}
				>
					View my orders
				</Button>
			</div>
			<div className="mb-1">
				<Button
					startIcon={<Edit />}
					variant="contained"
					color="primary"
					onClick={() => history.push('/profile/edit')}
				>
					Edit
				</Button>
			</div>
			<Button
				startIcon={<ExitToApp />}
				variant="contained"
				color="secondary"
				onClick={() => logout(dispatch, history)}
			>
				Logout
			</Button>
		</>
	)
}

export default MainProfile

import { TextField, Button } from '@material-ui/core'
import { useContext, useState } from 'react'
import { Camera, Lock, Edit } from '@material-ui/icons'
import { UserContext } from '../../Context'
import UserAPI from '../../api/user'
import { useHistory } from 'react-router'

const EditProfile = () => {
	document.title = 'Edit profile'
	let history = useHistory()
	const { auth } = useContext(UserContext)
	const { user } = auth
	const initialValue = {
		firstname: user.firstName,
		lastname: user.lastName,
		pseudo: user.username,
	}
	const [formData, setFormData] = useState(initialValue)

	/**
	 * Send to the API the new user information
	 * @param {Event} e
	 */
	const handleSubmit = (e) => {}

	return (
		<>
			<form>
				<h3>Edit profile</h3>
				<div className="mb-1">
					<TextField
						required
						value={formData.pseudo}
						onChange={(e) => setFormData({ ...formData, pseudo: e.target.value })}
						id="pseudo"
						label="Pseudo"
						variant="outlined"
					/>
				</div>
				<div className="mb-1">
					<TextField
						required
						value={formData.firstname}
						onChange={(e) => setFormData({ ...formData, firstname: e.target.value })}
						id="firstname"
						label="Firstname"
						variant="outlined"
					/>
				</div>
				<div className="mb-1">
					<TextField
						required
						value={formData.lastname}
						onChange={(e) => setFormData({ ...formData, lastname: e.target.value })}
						id="lastname"
						label="Lastname"
						variant="outlined"
					/>
				</div>
				<div className="mb-1">
					<Button variant="contained" color="primary" type="submit" startIcon={<Edit />}>
						Edit profile
					</Button>
				</div>
			</form>
			<div className="mb-1">
				<Button
					startIcon={<Camera />}
					variant="contained"
					color="primary"
					onClick={() => history.push('/profile/edit/picture')}
					style={{ marginRight: '1rem' }}
				>
					Change Picture
				</Button>
				<Button startIcon={<Lock />} variant="contained" color="primary">
					Change Password
				</Button>
			</div>
		</>
	)
}

export default EditProfile

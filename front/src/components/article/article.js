import { Typography } from "@material-ui/core"

const Article = ({ title, description, author }) => {
  return (
    <div>
      <Typography variant="h3">{title}</Typography>
      <Typography color="textSecondary">{author}</Typography>
      <Typography variant="body2" component="p">
        {description}
      </Typography>
    </div>
  )
}

export default Article

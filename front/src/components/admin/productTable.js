import {
	TableContainer,
	Table,
	TableHead,
	TableRow,
	TableCell,
	TableBody,
	Checkbox,
	Paper,
	TablePagination,
	IconButton,
	Toolbar,
	Typography,
	Tooltip,
	LinearProgress,
} from '@material-ui/core'
import { Delete, Edit } from '@material-ui/icons'
import { useState, useReducer } from 'react'
import { inialState, reducer } from '../../reducer/product'
import { getProducts } from '../../actions/product'
import { useEffect } from 'react'
import ProductAPI from '../../api/product'

const ProductTable = ({ history }) => {
	const [state, dispatch] = useReducer(reducer, inialState)
	const { data, page, isLoading, error } = state
	const [rowsPerPage, setRowsPerPage] = useState(5)
	const [selected, setSelected] = useState([])
	const [isAllChecked, setIsAllChecked] = useState(false)

	useEffect(() => {
		getProducts(dispatch, { page: 1 })
	}, [])

	useEffect(() => {
		if (error && page !== 1) getProducts(dispatch, { page: page - 1, limit: rowsPerPage })
	}, [error])

	const handleCheckOne = (id) => {
		const index = selected.indexOf(id)
		if (index === -1) {
			setSelected([...selected, id])
			return
		}
		const newSelected = [...selected]
		newSelected.splice(index, 1)
		setSelected(newSelected)
		if (isAllChecked) setIsAllChecked(false)
	}

	/**
	 * Select All product
	 * @param {Event} e
	 */
	const handleSelectAll = (e) => {
		const checked = !isAllChecked
		setIsAllChecked(checked)
		if (checked) {
			const selected = data.products.map((product) => product._id)
			setSelected(selected)
			return
		}
		setSelected([])
	}

	/**
	 * Change Page
	 * @param {Event} e
	 * @param {Number} newPage
	 */
	const handleChangePage = (e, newPage) => {
		getProducts(dispatch, { page: newPage + 1, limit: rowsPerPage })
		setIsAllChecked(false)
		setSelected([])
	}

	/**
	 * Change Rows Per Page
	 * @param {Event} e
	 */
	const handleChangeRowsPerPage = (e) => {
		const nbRow = parseInt(e.target.value, 10)
		setRowsPerPage(nbRow)
		getProducts(dispatch, { page: 1, limit: nbRow })
		setIsAllChecked(false)
		setSelected([])
	}

	const handleDelete = async () => {
		await ProductAPI.deleteMany(selected)
		await getProducts(dispatch, { page: page, limit: rowsPerPage })
		setSelected([])
		setIsAllChecked(false)
	}

	const isSelected = (id) => selected.indexOf(id) !== -1

	return (
		<>
			<Paper>
				<Toolbar>
					<Typography variant="h6" id="tableTitle" component="div" style={{ flex: '1 1 100%' }}>
						Products
					</Typography>
					<Tooltip title="Delete">
						<IconButton
							aria-label="delete"
							color={selected.length !== 0 ? 'secondary' : 'default'}
							onClick={handleDelete}
						>
							<Delete />
						</IconButton>
					</Tooltip>
				</Toolbar>
				{isLoading && <LinearProgress />}
				<TableContainer>
					<Table>
						<TableHead>
							<TableRow>
								<TableCell padding="checkbox">
									<Checkbox
										onChange={handleSelectAll}
										inputProps={{ 'aria-label': 'select all product' }}
										checked={isAllChecked}
									/>
								</TableCell>
								<TableCell>Title</TableCell>
								<TableCell>Quantity</TableCell>
								<TableCell>Price</TableCell>
								<TableCell>Edit</TableCell>
							</TableRow>
						</TableHead>
						<TableBody>
							{data?.products.map((product) => (
								<TableRow key={product._id}>
									<TableCell padding="checkbox">
										<Checkbox
											checked={isSelected(product._id)}
											onChange={() => handleCheckOne(product._id)}
										/>
									</TableCell>
									<TableCell>{product.title}</TableCell>
									<TableCell>{product.quantity}</TableCell>
									<TableCell>{product.price}€</TableCell>
									<TableCell>
										<label htmlFor="icon-edit-product">
											<IconButton
												aria-label="edit product"
												component="span"
												onClick={() => history.push(`/admin/product/edit/${product._id}`)}
											>
												<Edit />
											</IconButton>
										</label>
									</TableCell>
								</TableRow>
							))}
						</TableBody>
					</Table>
				</TableContainer>

				<TablePagination
					rowsPerPageOptions={[5, 10, 15]}
					component="div"
					count={data ? data._metadata.total_count : 3}
					rowsPerPage={rowsPerPage}
					page={page ? page - 1 : 0}
					onChangePage={handleChangePage}
					onChangeRowsPerPage={handleChangeRowsPerPage}
				/>
			</Paper>
		</>
	)
}

export default ProductTable

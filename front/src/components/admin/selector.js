import { MenuItem, FormControl, Select } from '@material-ui/core'

const Selector = ({ type, setType }) => {
	return (
		<FormControl>
			<Select
				labelId="demo-customized-select-label"
				id="demo-customized-select"
				value={type}
				onChange={(e) => setType(e.target.value)}
			>
				<MenuItem value={'Product'}>Product</MenuItem>
				<MenuItem value={'User'}>User</MenuItem>
				<MenuItem value={'Article'}>Article </MenuItem>
			</Select>
		</FormControl>
	)
}

export default Selector

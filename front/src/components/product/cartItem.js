import { Add, Remove } from '@material-ui/icons'
import { Grid, IconButton, Card } from '@material-ui/core'
import { ACTIONS } from '../../reducer/cart'
import useProgressiveImage from '../../hooks/useProgressiveImage'
import { Skeleton } from '@material-ui/lab'
import { memo } from 'react'

const CartItem = memo(({ item, dispatch }) => {
	const loaded = useProgressiveImage(item.product.pictures[0])
	return (
		<>
			<Card variant="outlined" style={{ margin: '2rem' }}>
				<Grid container spacing={4} style={{ width: '100%' }}>
					<Grid item xs={8}>
						{loaded && <img src={loaded} alt={item.product.title} height="275" />}
						{!loaded && (
							<Skeleton
								animation="wave"
								variant="rect"
								style={{
									height: 275,
								}}
							/>
						)}
					</Grid>
					<Grid item xs={4}>
						<div>
							<b>{item.product.title}</b>
						</div>
						<div>Price : {item.product.price} €</div>
						<div>
							Quantity :
							<IconButton
								aria-label="Remove one item"
								onClick={() => {
									dispatch({ type: ACTIONS.DECREASE_ITEM, payload: { product: item.product } })
								}}
							>
								<Remove />
							</IconButton>
							{item.quantity}
							<IconButton
								aria-label="Add one item"
								onClick={() => {
									dispatch({ type: ACTIONS.ADD_ITEM, payload: { product: item.product } })
								}}
							>
								<Add />
							</IconButton>
						</div>
						<div>
							Subtotal : <b>{Math.round(item.product.price * item.quantity * 100) / 100} €</b>
						</div>
					</Grid>
				</Grid>
			</Card>
		</>
	)
})

export default CartItem

import Product from './productCard'

const ProductList = ({ productList, setToasterVisible }) => {
	return (
		<>
			{productList
				? productList.map(
						(product) =>
							product.quantity !== 0 && (
								<Product
									product={product}
									key={product._id}
									setToasterVisible={setToasterVisible}
								/>
							)
				  )
				: null}
		</>
	)
}

export default ProductList

import { useState } from 'react'
import { useHistory } from 'react-router-dom'
import {
	Card,
	Typography,
	CardMedia,
	CardHeader,
	CardContent,
	CardActions,
	IconButton,
} from '@material-ui/core'
import { Skeleton } from '@material-ui/lab'
import { Visibility, ShoppingCart, Favorite, FavoriteBorder } from '@material-ui/icons'
import useProgressiveImage from '../../hooks/useProgressiveImage'
import { ACTIONS } from '../../reducer/cart'
import { useContext } from 'react'
import { CartContext } from '../../Context'

/**
 * Trim a string with [...] if sub to delimiter
 * @param {String} string
 * @param {Number} delimiter
 * @returns
 */
const trim = (string, delimiter) => {
	if (string.length <= delimiter) return string
	return string.substring(0, delimiter - 5) + '[...]'
}

const Product = ({ product, setToasterVisible }) => {
	let history = useHistory()
	const { dispatch } = useContext(CartContext)

	const { title, pictures, description, price, slug } = product
	const [favorite, setFavorite] = useState(false)
	const loaded = useProgressiveImage(pictures[0])

	return (
		<div>
			<Card style={{ width: 275, margin: '2rem' }}>
				<CardHeader title={trim(title, 75)} />
				{loaded && (
					<CardMedia
						style={{
							height: 0,
							paddingTop: '56.25%', // 16:9
						}}
						image={loaded}
						title={title}
					/>
				)}
				{!loaded && (
					<Skeleton
						animation="wave"
						variant="rect"
						style={{
							height: 0,
							paddingTop: '56.25%', // 16:9
						}}
					/>
				)}

				<CardContent>
					<Typography color="textSecondary">{price} €</Typography>
					<Typography variant="body2" component="p">
						{trim(description, 125)}
					</Typography>
				</CardContent>
				<CardActions disableSpacing>
					<IconButton aria-label="see more" onClick={() => history.push(`/e-commerce/${slug}`)}>
						<Visibility />
					</IconButton>
					<IconButton
						aria-label="shopping cart"
						onClick={() => {
							dispatch({ type: ACTIONS.ADD_ITEM, payload: { product } })
							setToasterVisible(true)
						}}
					>
						<ShoppingCart />
					</IconButton>
					<IconButton aria-label="favorite" onClick={() => setFavorite(!favorite)}>
						{favorite ? <Favorite /> : <FavoriteBorder />}
					</IconButton>
				</CardActions>
			</Card>
		</div>
	)
}

export default Product

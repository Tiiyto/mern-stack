const SuceedCheckout = ({ ticket }) => {
	return (
		<div>
			Your order is on the way
			<div>
				<a href={ticket} target="_blank" rel="noreferrer">
					RECEIPT
				</a>
			</div>
		</div>
	)
}

export default SuceedCheckout

import { Alert } from '@material-ui/lab'

const Toaster = ({ isVisible, setToasterVisible }) => {
	if (isVisible)
		return (
			<div
				style={{
					position: 'fixed',
					top: '5vw',
					left: '50%',
					transform: 'translateX(-50%)',
				}}
			>
				<div style={{ width: '50vw' }}>
					<Alert
						severity="success"
						onClose={() => {
							setToasterVisible(false)
						}}
					>
						Product added in the cart.
					</Alert>
				</div>
			</div>
		)
	else return <></>
}

export default Toaster

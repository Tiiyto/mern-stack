import { Card, CardContent, CardActions } from "@material-ui/core"
import { Skeleton } from "@material-ui/lab"

const ProductCardSkeleton = () => {
  return (
    <div>
      <Card style={{ width: 275, margin: "2rem" }}>
        <div
          style={{
            justifyContent: "center",
            alignItems: "center",
            display: "flex",
            flexDirection: "column",
          }}
        >
          <Skeleton
            animation="wave"
            height={30}
            width="80%"
            style={{ marginBottom: 1, marginTop: "1rem" }}
          />
          <Skeleton
            animation="wave"
            height={30}
            width="80%"
            style={{ marginBottom: 1 }}
          />
          <Skeleton
            animation="wave"
            height={30}
            width="80%"
            style={{ marginBottom: 5 }}
          />
        </div>
        <Skeleton
          animation="wave"
          variant="rect"
          style={{
            height: 0,
            paddingTop: "56.25%", // 16:9
          }}
        />
        <CardContent>
          <Skeleton animation="wave" height={10} style={{ marginBottom: 6 }} />
          <Skeleton animation="wave" height={10} style={{ marginBottom: 6 }} />
          <Skeleton animation="wave" height={10} style={{ marginBottom: 6 }} />
        </CardContent>
        <CardActions
          disableSpacing
          style={{
            justifyContent: "center",
            display: "flex",
            marginBottom: "1rem",
          }}
        >
          <Skeleton animation="wave" height={50} width="60%" />
        </CardActions>
      </Card>
    </div>
  )
}

export default ProductCardSkeleton

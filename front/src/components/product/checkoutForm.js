import { Button, CircularProgress } from '@material-ui/core'
import { CardElement, useStripe, useElements } from '@stripe/react-stripe-js'
import { useState } from 'react'
import { ACTIONS } from '../../reducer/cart'
import orderAPI from '../../api/order'
import { useContext } from 'react'
import { CartContext } from '../../Context'

const CheckoutForm = ({ amount, setTicket }) => {
	const stripe = useStripe()
	const elements = useElements()
	const [loading, setLoading] = useState(false)

	const { cart, dispatch } = useContext(CartContext)

	const handleSubmit = async (e) => {
		e.preventDefault()
		setLoading(true)
		const card = elements.getElement(CardElement)
		const { token } = await stripe.createToken(card)
		const response = await orderAPI.paid(amount * 100, cart, token.id, 'test@tes.fr')
		setLoading(false)
		dispatch({ type: ACTIONS.RESET_CART })
		setTicket(response)
	}

	return (
		<>
			<form onSubmit={handleSubmit} style={{ maxWidth: 350 }}>
				Price : {amount}
				<CardElement />
				<Button variant="contained" color="primary" type="submit">
					Pay
				</Button>
			</form>
			<CircularProgress style={loading ? {} : { display: 'none' }} />
		</>
	)
}

export default CheckoutForm

import {
	AppBar,
	Toolbar,
	Typography,
	Button,
	IconButton,
	Badge,
	useMediaQuery,
	Grid,
} from '@material-ui/core'
import { Avatar } from '@material-ui/core'
import { Home, Menu, ShoppingCart, Store, SupervisorAccount } from '@material-ui/icons'
import { useContext } from 'react'
import { Link, useHistory } from 'react-router-dom'
import { UserContext, CartContext } from '../Context'
import '../style/Navbar.css'

const NavBar = () => {
	let history = useHistory()

	const { auth } = useContext(UserContext)
	const { cart } = useContext(CartContext)

	const { user } = auth

	const nbItems = cart.items.reduce((prev, current) => prev + current.quantity, 0)
	const matches = useMediaQuery('(max-width:600px)')

	const handleUserClick = () => {
		if (user) return history.push('/profile')
		else return history.push('/login')
	}
	return (
		<div className="root">
			<AppBar position="static">
				<Toolbar>
					{matches && (
						<IconButton edge="start" color="inherit" aria-label="menu">
							<Menu />
						</IconButton>
					)}

					<Typography variant={matches ? 'h7' : 'h6'} className="title">
						<>
							<Link to="/" className="mr-1">
								<Grid container direction="row" alignItems="center">
									<Home />
									<span style={{ marginLeft: '0.5rem' }}>Home</span>
								</Grid>
							</Link>
							<Link to="/e-commerce" className="mr-1">
								<Grid container direction="row" alignItems="center">
									<Store />
									<span style={{ marginLeft: '0.5rem' }}>E-commerce</span>
								</Grid>
							</Link>
							{user?.isAdmin && (
								<Link to="/admin">
									<Grid container direction="row" alignItems="center">
										<SupervisorAccount />
										<span style={{ marginLeft: '0.5rem' }}> Admin</span>
									</Grid>
								</Link>
							)}
						</>
					</Typography>
					{nbItems !== 0 && (
						<Button onClick={() => history.push('/cart')}>
							<Badge badgeContent={nbItems} color="secondary">
								<ShoppingCart style={{ fill: 'white' }} />
							</Badge>
						</Button>
					)}
					<Button color="inherit" onClick={handleUserClick}>
						{!matches && (
							<Typography variant="inherit" className="mr-1">
								{user && user.givenName}
							</Typography>
						)}

						{user ? <Avatar alt={user.name} src={user.imageUrl}></Avatar> : 'Login'}
					</Button>
				</Toolbar>
			</AppBar>
		</div>
	)
}

export default NavBar

import { useState } from 'react'
import { Button, Chip, Paper, TextField, Typography } from '@material-ui/core'
import productAPI from '../../api/product'
import { useHistory } from 'react-router-dom'

const NewProduct = () => {
	document.title = 'New Product'

	let history = useHistory()
	const initialValue = { title: '', price: 0, description: '', quantity: 0, pictureURL: [] }
	const [product, setProduct] = useState(initialValue)
	const [url, setUrl] = useState('')

	const handleAddChip = (e) => {
		if (e.key === 'Enter') {
			e.preventDefault()
			setProduct({ ...product, pictureURL: [...product.pictureURL, e.target.value] })
			setUrl('')
		}
	}

	const handleRemoveChip = (urlToRemove) => {
		const newUrl = product.pictureURL.filter((url) => url !== urlToRemove)
		setProduct({ ...product, pictureURL: newUrl })
	}

	/**
	 *
	 * @param {Event} e
	 * @returns
	 */
	const handleSubmit = async (e) => {
		e.preventDefault()
		try {
			await productAPI.create(
				product.title,
				product.price,
				product.description,
				product.quantity,
				product.pictureURL
			)
			return history.push('/e-commerce')
		} catch (err) {
			console.log(err)
		}
	}

	return (
		<>
			<form onSubmit={handleSubmit}>
				<Typography variant="h4">New Product</Typography>
				<div className="mb-1">
					<TextField
						required
						value={product.title}
						onChange={(e) => setProduct({ ...product, title: e.target.value })}
						id="title"
						label="Title"
					/>
				</div>
				<div className="mb-1">
					<TextField
						required
						value={product.price}
						onChange={(e) => setProduct({ ...product, price: e.target.value })}
						id="price"
						label="Price"
						type="number"
					/>
				</div>
				<div className="mb-1">
					<TextField
						required
						value={product.description}
						multiline
						onChange={(e) => setProduct({ ...product, description: e.target.value })}
						id="description"
						label="Description"
					/>
				</div>
				<div className="mb-1">
					<TextField
						required
						value={product.quantity}
						type="number"
						onChange={(e) => setProduct({ ...product, quantity: e.target.value })}
						id="quantity"
						label="Quantity"
					/>
				</div>
				<div className="mb-1">
					<TextField
						value={url}
						onChange={(e) => setUrl(e.target.value)}
						onKeyPress={handleAddChip}
						id="pictureURL"
						label="Picture URL"
					/>
				</div>
				<div className="mb-1">
					<Paper
						component="ul"
						style={{
							display: 'flex',
							justifyContent: 'center',
							flexWrap: 'wrap',
							listStyle: 'none',
							margin: 0,
						}}
					>
						{product.pictureURL.map((url) => (
							<li>
								<Chip
									label={url}
									onDelete={() => handleRemoveChip(url)}
									style={{ margin: '0.5rem' }}
								/>
							</li>
						))}
					</Paper>
				</div>
				<Button variant="contained" color="primary" type="submit">
					Create
				</Button>
			</form>
		</>
	)
}

export default NewProduct

import { useEffect, useState } from 'react'
import { useParams } from 'react-router'
import { Button, Chip, Paper, TextField, Typography } from '@material-ui/core'
import productAPI from '../../api/product'
import { useHistory } from 'react-router-dom'

const EditProduct = () => {
	document.title = 'Edit Product'
	const { id } = useParams()
	let history = useHistory()
	const initialValue = { title: '', price: 0, description: '', quantity: 0, pictureURL: [] }
	const [product, setProduct] = useState(initialValue)
	const [url, setUrl] = useState('')

	useEffect(() => {
		const getProduct = async () => {
			const product = await productAPI.getProductByID(id)
			setProduct({
				title: product.title,
				price: product.price,
				description: product.description,
				quantity: product.quantity,
				pictureURL: product.pictures,
			})
		}
		getProduct()
	}, [id])

	/**
	 *
	 * @param {Event} e
	 * @returns
	 */
	const handleSubmit = async (e) => {
		e.preventDefault()
		try {
			await productAPI.update(product, id)
			return history.push('/e-commerce')
		} catch (err) {
			console.log(err)
		}
	}

	/**
	 * Add a picture URL to the array product.pictureURL
	 * @param {Event} e
	 */
	const handleAddChip = (e) => {
		if (e.key === 'Enter') {
			e.preventDefault()
			setProduct({ ...product, pictureURL: [...product.pictureURL, e.target.value] })
			setUrl('')
		}
	}

	/**
	 * Remove an URL from the array prodcut.pictureURL
	 * @param {String} urlToRemove
	 */
	const handleRemoveChip = (urlToRemove) => {
		const newUrl = product.pictureURL.filter((url) => url !== urlToRemove)
		setProduct({ ...product, pictureURL: newUrl })
	}

	return (
		<form onSubmit={handleSubmit}>
			<Typography variant="h4">Edit Product</Typography>
			<div className="mb-1">
				<TextField
					required
					value={product.title}
					onChange={(e) => setProduct({ ...product, title: e.target.value })}
					id="title"
					label="Title"
				/>
			</div>
			<div className="mb-1">
				<TextField
					required
					value={product.price}
					onChange={(e) => setProduct({ ...product, price: e.target.value })}
					id="price"
					label="Price"
					type="number"
				/>
			</div>
			<div className="mb-1">
				<TextField
					required
					value={product.description}
					multiline
					onChange={(e) => setProduct({ ...product, description: e.target.value })}
					id="description"
					label="Description"
				/>
			</div>
			<div className="mb-1">
				<TextField
					required
					value={product.quantity}
					type="number"
					onChange={(e) => setProduct({ ...product, quantity: e.target.value })}
					id="quantity"
					label="Quantity"
				/>
			</div>
			<div className="mb-1">
				<TextField
					value={url}
					onChange={(e) => setUrl(e.target.value)}
					onKeyPress={handleAddChip}
					id="pictureURL"
					label="Picture URL"
				/>
			</div>
			<div className="mb-1">
				<Paper
					component="ul"
					style={{
						display: 'flex',
						justifyContent: 'center',
						flexWrap: 'wrap',
						listStyle: 'none',
						margin: 0,
					}}
				>
					{product.pictureURL.map((url) => (
						<li>
							<Chip
								label={url}
								onDelete={() => handleRemoveChip(url)}
								style={{ margin: '0.5rem' }}
							/>
						</li>
					))}
				</Paper>
			</div>
			<Button variant="contained" color="primary" type="submit">
				Update
			</Button>
		</form>
	)
}

export default EditProduct

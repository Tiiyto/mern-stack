const NewArticle = () => {
	return (
		<form>
			<h3>New Article</h3>
			<div>
				<label htmlFor="title">Title</label>
				<input type="text" />
			</div>
			<div>
				<label htmlFor="content">Content</label>
				<textarea id="content" />
			</div>
			<button>Create new Article</button>
		</form>
	)
}

export default NewArticle

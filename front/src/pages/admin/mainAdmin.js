import { useHistory } from 'react-router-dom'
import { Button } from '@material-ui/core'
import { useState } from 'react'
import Selector from '../../components/admin/selector'
import ProductTable from '../../components/admin/productTable'
import UserTable from '../../components/admin/userTable'
import ArticleTable from '../../components/admin/articleTable'

const MainAdmin = () => {
	document.title = 'Admin page'
	let history = useHistory()
	const [type, setType] = useState('Product')

	const table = (type) => {
		switch (type) {
			case 'Product':
				return <ProductTable history={history} />
			case 'User':
				return <UserTable history={history} />
			case 'Article':
				return <ArticleTable history={history} />
			default:
				return <ProductTable history={history} />
		}
	}

	return (
		<>
			<div style={{ display: 'flex', margin: '10px', marginBottom: '0' }}>
				<Selector
					type={type}
					setType={setType}
					style={{
						marginLeft: 'auto',
					}}
				/>
				<Button
					variant="contained"
					color="primary"
					onClick={() => history.push(`/admin/${type.toLowerCase()}/new`)}
					style={{
						marginLeft: 'auto',
					}}
				>
					Create New {type}
				</Button>
			</div>
			{table(type)}
		</>
	)
}
export default MainAdmin

import { useState } from 'react'
import { useHistory } from 'react-router-dom'
import AuthAPI from '../../api/auth'
import { TextField, CircularProgress, Button } from '@material-ui/core'
import '../../style/Style.css'

const ResetPassword = () => {
	document.title = 'Reset Password'
	const [emailAddress, setEmailAddress] = useState('')
	const [loading, setLoading] = useState(false)
	const [error, setError] = useState(null)
	let history = useHistory()

	/**
	 *
	 * @param {Event} e
	 */
	const handleEmailChange = (e) => {
		setEmailAddress(e.target.value)
		if (error) setError(false)
	}

	/**
	 *
	 * @param {Event} e
	 */
	const handleSubmit = async (e) => {
		e.preventDefault()
		setLoading(true)
		try {
			const response = await AuthAPI.resetPassword(emailAddress)
			console.log(response)
			history.push('/login')
		} catch (err) {
			setError(true)
		}
		setLoading(false)
	}

	return (
		<>
			<form onSubmit={handleSubmit}>
				<h3>Reset Password</h3>
				<div className="mb-1">
					<TextField
						error={error ? true : false}
						required
						value={emailAddress}
						onChange={handleEmailChange}
						id="email"
						autoComplete="email"
						label="Email"
						type="email"
						variant="outlined"
					/>
				</div>
				<div className="mb-1">
					<Button variant="contained" color="primary" type="submit">
						Reset Password
					</Button>
				</div>
			</form>
			<div className="mb-1">
				<CircularProgress style={loading ? {} : { display: 'none' }} />
			</div>
		</>
	)
}

export default ResetPassword

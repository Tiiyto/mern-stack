import { useState } from 'react'
import { useHistory } from 'react-router-dom'
import UserAPI from '../../api/user'
import '../../style/Style.css'
import { TextField, CircularProgress, Button } from '@material-ui/core'

const Register = () => {
	let history = useHistory()
	document.title = 'Register'

	const initialValue = {
		username: '',
		password: '',
		firstname: '',
		lastname: '',
		emailaddress: '',
		passwordCheck: '',
	}
	const [formData, setFormData] = useState(initialValue)
	const [passwordCheck, setPasswordCheck] = useState('')
	const [isPasswordSame, setIsPasswordSame] = useState(true)
	const [usernameError, setUsernameError] = useState(null)
	const [loading, setLoading] = useState(false)

	/**
	 *
	 * @param {Event} e
	 */
	const onUsernameChange = (e) => {
		setFormData({ ...formData, username: e.target.value })
		setUsernameError(null)
	}

	/**
	 *
	 * @param {Event} e
	 */
	const onPasswordCheckChange = (e) => {
		const passwordCheckTemp = e.target.value
		setPasswordCheck(passwordCheckTemp)
		if (passwordCheckTemp === '') setIsPasswordSame(true)
		else {
			if (formData.password === passwordCheckTemp) setIsPasswordSame(true)
			else setIsPasswordSame(false)
		}
	}

	/**
	 *
	 * @param {Event} e
	 */
	const handleSubmit = async (e) => {
		e.preventDefault()
		if (isPasswordSame) {
			setLoading(true)
			const { username, password, firstname, lastname, emailaddress } = formData
			const response = await UserAPI.register(username, password, firstname, lastname, emailaddress)
			setLoading(false)
			if (response.status === 200) {
				history.push('/login')
			} else {
				if (response.status === 400) alert('data missed')
				if (response.status === 409) setUsernameError('Username already exists')
			}
		}
	}

	return (
		<>
			<form onSubmit={handleSubmit}>
				<h3>Register</h3>
				<div className="mb-1">
					<TextField
						required
						error={usernameError ? true : false}
						helperText={usernameError}
						value={formData.username}
						onChange={onUsernameChange}
						id="username"
						label="Username"
						variant="outlined"
					/>
				</div>
				<div className="mb-1">
					<TextField
						required
						value={formData.emailaddress}
						onChange={(e) => setFormData({ ...formData, emailaddress: e.target.value })}
						id="email"
						autoComplete="email"
						label="Email"
						type="email"
						variant="outlined"
					/>
				</div>
				<div className="mb-1">
					<TextField
						required
						value={formData.firstname}
						onChange={(e) => setFormData({ ...formData, firstname: e.target.value })}
						id="firstname"
						label="First name"
						variant="outlined"
					/>
				</div>
				<div className="mb-1">
					<TextField
						required
						value={formData.lastname}
						onChange={(e) => setFormData({ ...formData, lastname: e.target.value })}
						id="lastname"
						label="Last name"
						variant="outlined"
					/>
				</div>
				<div className="mb-1">
					<TextField
						error={!isPasswordSame}
						required
						value={formData.password}
						onChange={(e) => setFormData({ ...formData, password: e.target.value })}
						id="password"
						label="Password"
						type="password"
						variant="outlined"
					/>
				</div>
				<div className="mb-1">
					<TextField
						error={!isPasswordSame}
						required
						value={passwordCheck}
						onChange={onPasswordCheckChange}
						id="passwordCheck"
						label="Retype Password"
						type="password"
						variant="outlined"
					/>
				</div>
				<div className="mb-1">
					<Button variant="contained" color="primary" type="submit">
						Register
					</Button>
				</div>
			</form>
			<div>
				<CircularProgress style={!loading && { display: 'none' }} />
			</div>
		</>
	)
}

export default Register

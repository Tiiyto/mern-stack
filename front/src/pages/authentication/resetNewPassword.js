import { useState } from 'react'
import { useHistory } from 'react-router-dom'
import { useParams } from 'react-router-dom'
import AuthAPI from '../../api/auth'
import { TextField, CircularProgress, Button } from '@material-ui/core'
import '../../style/Style.css'

const ResetPasswordNew = () => {
	document.title = 'Reset Password'
	const { token } = useParams()
	const [password, setPassword] = useState('')
	const [passwordCheck, setPasswordCheck] = useState('')
	const [isPasswordSame, setIsPasswordSame] = useState(true)
	const [loading, setLoading] = useState(false)
	let history = useHistory()

	/**
	 *
	 *
	 * @param {Event} e
	 */
	const onPasswordChange = (e) => {
		setPassword(e.target.value)
	}

	/**
	 *
	 * @param {Event} e
	 */
	const onPasswordCheckChange = (e) => {
		const passwordCheckTemp = e.target.value
		setPasswordCheck(passwordCheckTemp)
		if (passwordCheckTemp === '') setIsPasswordSame(true)
		else {
			if (password === passwordCheckTemp) setIsPasswordSame(true)
			else setIsPasswordSame(false)
		}
	}

	/**
	 *
	 * @param {Event} e
	 */
	const handleSubmit = async (e) => {
		e.preventDefault()
		setLoading(true)
		try {
			AuthAPI.resetPasswordDone(token, password)
			history.push('/login')
		} catch (err) {
			console.error(err)
		}
		setLoading(false)
	}

	return (
		<>
			<form onSubmit={handleSubmit}>
				<h3>Reset Password</h3>
				<div className="mb-1">
					<TextField
						error={!isPasswordSame}
						required
						value={password}
						onChange={onPasswordChange}
						id="password"
						label="Password"
						type="password"
						variant="outlined"
					/>
				</div>
				<div className="mb-1">
					<TextField
						error={!isPasswordSame}
						required
						value={passwordCheck}
						onChange={onPasswordCheckChange}
						id="passwordCheck"
						label="Retype Password"
						type="password"
						variant="outlined"
					/>
				</div>
				<div className="mb-1">
					<Button variant="contained" color="primary" type="submit">
						Reset Password
					</Button>
				</div>
			</form>
			<div className="mb-1">
				<CircularProgress style={loading ? {} : { display: 'none' }} />
			</div>
		</>
	)
}

export default ResetPasswordNew

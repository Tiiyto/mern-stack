import { useContext, useState } from 'react'
import { Link, useHistory } from 'react-router-dom'
import { TextField, CircularProgress, Button, Typography } from '@material-ui/core'
import { GoogleLogin } from 'react-google-login'
import '../../style/Style.css'
import { UserContext } from '../../Context'
import { signIn, signInGoogle } from '../../actions/auth'

const Login = () => {
	document.title = 'Login'
	let history = useHistory()
	const { dispatch, auth } = useContext(UserContext)
	const { isLoading, error } = auth

	const [username, setUsername] = useState('')
	const [password, setPassword] = useState('')

	/**
	 *
	 * @param {Event} e
	 */
	const handleSubmit = async (e) => {
		e.preventDefault()
		signIn(dispatch, { username, password })
	}

	const errorGoogle = (response) => {
		console.log(response)
	}

	return (
		<>
			<form onSubmit={handleSubmit}>
				<h3>Login</h3>
				<div className="mb-1">
					<TextField
						error={error ? true : false}
						required
						value={username}
						onChange={(e) => setUsername(e.target.value)}
						id="username"
						label="Username"
						variant="outlined"
					/>
				</div>
				<div>
					<TextField
						error={error ? true : false}
						required
						value={password}
						onChange={(e) => setPassword(e.target.value)}
						id="password"
						label="Password"
						type="password"
						variant="outlined"
					/>
				</div>
				<div className="mb-1">
					<Typography variant="caption">
						<Link to="/reset-password">password forgotten</Link>
					</Typography>
				</div>
				<div className="mb-1">
					<Button variant="contained" color="primary" type="submit">
						Login
					</Button>
				</div>
			</form>
			<div className="mb-1">
				<CircularProgress style={isLoading ? {} : { display: 'none' }} />
			</div>
			<div className="mb-1">
				<Button variant="contained" color="primary" onClick={() => history.push('/register')}>
					Register
				</Button>
			</div>
			<div className="mb-1">
				<GoogleLogin
					clientId={process.env.REACT_APP_CLIENT_ID}
					onSuccess={(response) => signInGoogle(dispatch, response, history)}
					onFailure={errorGoogle}
					cookiePolicy={'single_host_origin'}
				/>
			</div>
		</>
	)
}

export default Login

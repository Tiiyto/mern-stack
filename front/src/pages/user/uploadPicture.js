import { Button, IconButton, Collapse, Avatar } from '@material-ui/core'
import { useContext, useState } from 'react'
import Alert from '@material-ui/lab/Alert'
import { useHistory } from 'react-router-dom'
import CloseIcon from '@material-ui/icons/Close'
import UserAPI from '../../api/user'
import { UserContext } from '../../Context'
import { ACTIONS } from '../../reducer/user'

const Picture = () => {
	document.title = 'Upload picture'
	const { dispatch } = useContext(UserContext)
	const [file, setFile] = useState(null)
	const [preview, setPreview] = useState(null)
	const [error, setError] = useState(false)
	const [errorText, setErrorText] = useState('')
	let history = useHistory()

	/**
	 *
	 * @param {Event} e
	 */
	const handleChangePicture = (e) => {
		setError(null)
		setFile(e.target.files[0])
		setPreview(URL.createObjectURL(e.target.files[0]))
	}

	/**
	 *
	 * @param {Event} e
	 */
	const handleSubmit = async (e) => {
		e.preventDefault()
		if (file) {
			const formData = new FormData()
			formData.append('file', file)
			const { user } = await UserAPI.picture(formData)
			dispatch({ type: ACTIONS.LOGGED, payload: user })
			history.push('/profile')
		} else {
			setError(true)
			setErrorText('Please insert a picture.')
		}
	}

	return (
		<>
			<Collapse in={error}>
				<Alert
					severity="error"
					action={
						<IconButton
							aria-label="close"
							color="inherit"
							size="small"
							onClick={() => {
								setError(false)
							}}
						>
							<CloseIcon fontSize="inherit" />
						</IconButton>
					}
				>
					{errorText}
				</Alert>
			</Collapse>
			<form onSubmit={handleSubmit}>
				<Collapse in={preview ? true : false}>
					<div style={{ justifyContent: 'center', display: 'flex' }}>
						<Avatar
							alt="Preview profile picture"
							src={preview}
							style={{ width: '10rem', height: '10rem' }}
						/>
					</div>
				</Collapse>
				<div>
					<input type="file" accept="image/*" name="file" onChange={handleChangePicture} />
				</div>
				<Button variant="contained" color="primary" type="submit">
					Submit
				</Button>
			</form>
		</>
	)
}

export default Picture

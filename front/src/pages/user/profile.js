import { useContext } from 'react'
import '../../style/Style.css'
import GoogleProfile from '../../components/user/googleProfile'
import MainProfile from '../../components/user/mainProfile'
import { UserContext } from '../../Context'

const Profile = () => {
	document.title = 'Profile page'
	const { auth } = useContext(UserContext)
	return (
		<>
			<h2>Profile</h2>
			{auth.user.googleId ? <GoogleProfile /> : <MainProfile />}
		</>
	)
}

export default Profile

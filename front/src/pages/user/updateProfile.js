import '../../style/Style.css'
import {TextField, CircularProgress, Button} from '@material-ui/core'

const UpdateProfile = () => {
    document.title = 'Update profile'

    return(
        <>
        <form onSubmit={}>
            <h3>Update profile</h3>
            <div className='mb-1'>
                <TextField required error={ usernameError ? true : false } helperText={usernameError} value={username} onChange={onUsernameChange} id='username' label="Username" variant="outlined" />
            </div>
            <div className='mb-1' >
                <Button variant="contained" color="primary" type="submit">Update</Button>
            </div>
        </form>
        <div>
            <CircularProgress style={loading ? {} : {display: "none"}} />
        </div>
        </>
    )
}

export default UpdateProfile
import ProductAPI from '../../api/product'
import { useHistory, useParams } from 'react-router-dom'
import { useEffect, useState } from 'react'
import {
	Typography,
	Grid,
	Paper,
	Button,
	FormControl,
	InputLabel,
	Select,
	MenuItem,
} from '@material-ui/core'
import { ACTIONS } from '../../reducer/cart'
import { CartContext } from '../../Context'
import { useContext } from 'react'
import { Alert } from '@material-ui/lab'
import Carousel from 'react-material-ui-carousel'

const ProductPage = () => {
	const { slug } = useParams()
	const [product, setProduct] = useState(null)
	const [nbItems, setNbItems] = useState(1)
	let history = useHistory()
	const { dispatch } = useContext(CartContext)

	useEffect(() => {
		const fecthProduct = async () => {
			const product = await ProductAPI.getProductBySlug(slug)
			setProduct(product)
		}
		fecthProduct()
	}, [slug])

	const handleAddCart = () => {
		dispatch({ type: ACTIONS.ADD_ITEM, payload: { product, quantity: nbItems } })
		history.push('/cart')
	}

	return (
		<>
			<Typography variant="h5">{product?.title}</Typography>
			<Grid container spacing={4} style={{ width: '100%' }}>
				<Grid item xs={6}>
					<Paper>
						<Carousel
							autoPlay={false}
							indicators={product?.pictures.length === 1 ? false : true}
							navButtonsAlwaysInvisible={product?.pictures.length === 1}
						>
							{product?.pictures.map((pictureURL) => (
								<img src={pictureURL} alt={product?.title} width="60%" />
							))}
						</Carousel>
					</Paper>
				</Grid>
				<Grid item xs={6}>
					<Paper>
						{product?.description}
						<br />
						{product?.price} €
						<br />
						{product?.quantity !== 0 && (
							<>
								<FormControl variant="outlined" style={{ margin: '1rem' }}>
									<InputLabel id="select-label">Quantity</InputLabel>
									<Select
										labelId="demo-simple-select-outlined-label"
										id="select"
										value={nbItems}
										onChange={(e) => setNbItems(e.target.value)}
										label="Quantity"
									>
										{[...new Array(product?.quantity).keys()].slice(0, 10).map((x) => (
											<MenuItem value={x + 1} key={x}>
												{x + 1}
											</MenuItem>
										))}
									</Select>
								</FormControl>
								<FormControl variant="outlined" style={{ margin: '1rem' }}>
									<Button variant="contained" color="primary" onClick={handleAddCart}>
										Add to Cart
									</Button>
								</FormControl>
							</>
						)}
						{product?.quantity === 0 && (
							<Alert variant="filled" severity="error">
								Out of stock.
							</Alert>
						)}
					</Paper>
				</Grid>
			</Grid>
		</>
	)
}

export default ProductPage

import { useHistory } from 'react-router-dom'
import { Button, Card, Grid } from '@material-ui/core'
import CartItem from '../../components/product/cartItem'
import { ACTIONS } from '../../reducer/cart'
import { CartContext } from '../../Context'
import { useContext } from 'react'

const aroundPrice = (price) => {
	return Math.round(price * 100) / 100
}

const Cart = () => {
	let history = useHistory()
	const { cart, dispatch } = useContext(CartContext)

	const nbItems = cart.items.reduce((prev, current) => prev + current.quantity, 0)
	const totalPrice = cart.items.reduce(
		(prev, current) => prev + current.quantity * current.product.price,
		0
	)
	const handleEmptyCart = () => {
		dispatch({ type: ACTIONS.RESET_CART })
		history.push('/e-commerce')
	}

	return (
		<>
			<Grid container spacing={4} style={{ width: '100%' }}>
				<Grid item xs={8}>
					{cart.items &&
						cart.items.map((item) => (
							<CartItem item={item} key={item.product._id} dispatch={dispatch} />
						))}
				</Grid>
				<Grid item xs={4}>
					<Card variant="outlined" style={{ margin: '2rem' }}>
						<div>
							Subtotal ({nbItems} items): <b>{aroundPrice(totalPrice)} €</b>
						</div>
						<Button
							variant="contained"
							color="primary"
							onClick={() => history.push('/checkout')}
							style={{ margin: '2rem' }}
						>
							Checkout
						</Button>
						<Button variant="contained" color="primary" onClick={handleEmptyCart}>
							Empty the cart
						</Button>
					</Card>
				</Grid>
			</Grid>
		</>
	)
}

export default Cart

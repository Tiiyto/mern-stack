import { Elements } from '@stripe/react-stripe-js'
import { loadStripe } from '@stripe/stripe-js'
import { useState, useEffect } from 'react'
import { useHistory } from 'react-router-dom'
import CheckoutForm from '../../components/product/checkoutForm'
import SuceedCheckout from '../../components/product/suceedCheckout'
import { useContext } from 'react'
import { CartContext } from '../../Context'

const stripePromise = loadStripe(process.env.REACT_APP_STRIPE_PUBLISHABLE_KEY)

const Checkout = () => {
	const { cart } = useContext(CartContext)
	const amount = cart.items.reduce(
		(prev, current) => prev + current.quantity * current.product.price,
		0
	)
	const [ticket, setTicket] = useState(null)
	let history = useHistory()
	useEffect(() => {
		if (!amount || amount === 0) history.goBack()
	}, [])
	if (ticket) return <SuceedCheckout ticket={ticket} />
	return (
		<>
			<Elements stripe={stripePromise}>
				<CheckoutForm amount={amount} setTicket={setTicket} />
			</Elements>
		</>
	)
}

export default Checkout

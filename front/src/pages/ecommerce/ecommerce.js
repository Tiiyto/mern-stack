import ProductList from '../../components/product/productList'
import Toaster from '../../components/product/toaster'
import { useEffect, useState, useReducer } from 'react'
import { inialState, reducer } from '../../reducer/product'
import { Pagination } from '@material-ui/lab'
import { getProducts } from '../../actions/product'
import ProductCardSkeleton from '../../components/product/cardSkeleton'
import { parse } from 'query-string'
import { useLocation } from 'react-router'

const ECommerce = () => {
	document.title = 'E-commerce'
	const location = useLocation()

	const [state, dispatch] = useReducer(reducer, inialState)
	const { isLoading, data, page } = state

	useEffect(() => {
		const currentPage = parseInt(parse(location.search).page)
			? parseInt(parse(location.search).page)
			: 1
		getProducts(dispatch, { page: currentPage })
	}, [location])

	let skeletons = []
	for (var i = 0; i < 5; i++) {
		skeletons.push(<ProductCardSkeleton key={i} />)
	}
	const [isToasterVisible, setToasterVisible] = useState(false)

	useEffect(() => {
		if (isToasterVisible) {
			setTimeout(() => {
				setToasterVisible(false)
			}, 1500)
		}
	}, [isToasterVisible])

	return (
		<>
			<h2>E-commerce</h2>
			<Toaster isVisible={isToasterVisible} setToasterVisible={setToasterVisible} />
			<div
				style={{
					justifyContent: 'center',
					flexWrap: 'wrap',
					alignItems: 'center',
					display: 'flex',
				}}
			>
				{isLoading ? (
					skeletons
				) : (
					<ProductList productList={data?.products} setToasterVisible={setToasterVisible} />
				)}
			</div>
			<div
				style={{
					justifyContent: 'center',
					display: 'flex',
					margin: '2rem',
				}}
			>
				<Pagination
					count={data ? data._metadata.page_count : 3}
					shape="rounded"
					page={page ? page : 1}
					onChange={(event, value) => {
						getProducts(dispatch, { page: value }, true)
					}}
				/>
			</div>
		</>
	)
}

export default ECommerce

import {
	IconButton,
	Paper,
	Table,
	TableBody,
	TableCell,
	TableContainer,
	TableHead,
	TableRow,
	Typography,
} from '@material-ui/core'
import { Visibility } from '@material-ui/icons'
import { useEffect, useState } from 'react'
import orderAPI from '../../api/order'
import moment from 'moment'

const Order = () => {
	document.title = 'Orders'

	const [orders, setOrders] = useState(null)

	useEffect(() => {
		const getOrders = async () => {
			try {
				const orders = await orderAPI.getOrders()
				if (orders.length !== 0) setOrders(orders)
			} catch (err) {
				console.log(err)
			}
		}
		getOrders()
	}, [])

	return (
		<>
			<Typography variant="h4">Orders</Typography>
			{!orders && "You don't have any orders."}
			{orders && (
				<TableContainer component={Paper}>
					<Table>
						<TableHead>
							<TableRow>
								<TableCell>Date</TableCell>
								<TableCell>Amount</TableCell>
								<TableCell>Receipt</TableCell>
							</TableRow>
						</TableHead>
						<TableBody>
							{orders.map((order) => (
								<TableRow>
									<TableCell>{moment(order.created_at).format('YYYY-MM-DD')}</TableCell>
									<TableCell>{order.amount}</TableCell>
									<TableCell>
										<IconButton aria-label="see receipt" onClick={() => window.open(order.receipt)}>
											<Visibility />
										</IconButton>
									</TableCell>
								</TableRow>
							))}
						</TableBody>
					</Table>
				</TableContainer>
			)}
		</>
	)
}

export default Order

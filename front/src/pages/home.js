import { useEffect, useState } from 'react'
import { useHistory } from 'react-router-dom'
import ArticleAPI from '../api/article'
import Article from '../components/article/article'
import '../style/Style.css'

const Home = () => {
	document.title = 'Home'

	let history = useHistory()
	const [articles, setArticles] = useState(null)

	const handleOnClick = (e) => {
		return history.push('/article/new')
	}

	useEffect(() => {
		ArticleAPI.getAllArticles().then((data) => setArticles(data))
	}, [])

	return (
		<div>
			aaaa
			{articles
				? articles.map((article) => (
						<Article
							title={article.title}
							key={article._id}
							description={article.description}
							author={article.author}
						/>
				  ))
				: null}
		</div>
	)
}

export default Home

import AuthAPI from '../api/auth'

const ACTIONS = {
	LOADING: 'loading',
	LOGGED_ERROR: 'error',
	LOGGED: 'logged',
	CHANGE: 'change',
	LOGOUT: 'logout',
}
const TOKEN_KEY = 'auth-token'
const initialState = { user: null, isLoading: false, error: false }

const init = (initialValue) => {
	if (localStorage.getItem(TOKEN_KEY)) {
		const user = AuthAPI.getCurrentUser()
		return { ...initialValue, user }
	}
	return initialValue
}

const reducer = (state, action) => {
	switch (action.type) {
		case ACTIONS.LOADING:
			return { ...state, isLoading: true, error: false }
		case ACTIONS.LOGGED:
			return { ...state, isLoading: false, error: false, user: action.payload }
		case ACTIONS.CHANGE:
			return { ...state, user: action.payload }
		case ACTIONS.LOGGED_ERROR:
			return { ...state, isLoading: false, error: true }
		case ACTIONS.LOGOUT:
			return { ...state, user: null }
		default:
			return state
	}
}

export { ACTIONS, reducer, init, initialState }

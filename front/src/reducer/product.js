const ACTIONS = {
	FETCH_DATA_REQUEST: 'fetch-data-request',
	FETCH_DATA_RESPONSE: 'fetch-data-response',
	ERROR: 'error',
}

const inialState = { data: null, page: null, isLoading: false, error: false }

const reducer = (state, action) => {
	switch (action.type) {
		case ACTIONS.FETCH_DATA_REQUEST:
			return { ...state, isLoading: true, error: false, page: action.payload }
		case ACTIONS.FETCH_DATA_RESPONSE:
			return { ...state, isLoading: false, data: action.payload }
		case ACTIONS.ERROR:
			return { ...state, isLoading: false, error: true, data: null }
		default:
			return state
	}
}

export { ACTIONS, inialState, reducer }

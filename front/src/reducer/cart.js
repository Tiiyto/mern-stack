const ACTIONS = {
	ADD_ITEM: 'add-item',
	ADD_ITEMS: 'add-items',
	DECREASE_ITEM: 'decrease-item',
	RESET_CART: 'reset-cart',
}

const initialState = { items: [] }

const init = (initialValue) => {
	if (localStorage.getItem('cart')) return JSON.parse(localStorage.getItem('cart'))
	return initialValue
}

const reducer = (cart, action) => {
	switch (action.type) {
		case ACTIONS.ADD_ITEM: {
			const { product, quantity = 1 } = action.payload
			const itemInCart = cart.items.find((item) => item.product._id === product._id)
			let newQuantity
			if (itemInCart) {
				return {
					items: cart.items.map((item) => {
						if (item.product.quantity < quantity + itemInCart.quantity) {
							newQuantity = item.product.quantity
						} else {
							newQuantity = itemInCart.quantity + quantity
						}
						return item.product === itemInCart.product
							? { product: itemInCart.product, quantity: newQuantity }
							: item
					}),
				}
			}
			return { items: [...cart.items, { product, quantity: quantity }] }
		}
		case ACTIONS.DECREASE_ITEM: {
			const { product, quantity = 1 } = action.payload
			const itemInCart = cart.items.find((item) => item.product._id === product._id)
			if (!itemInCart) return cart
			if (itemInCart.quantity - quantity <= 0)
				return {
					items: cart.items.filter((item) => item.product._id !== product._id),
				}
			return {
				items: cart.items.map((item) =>
					item.product === itemInCart.product
						? { product: itemInCart.product, quantity: itemInCart.quantity - quantity }
						: item
				),
			}
		}

		case ACTIONS.RESET_CART: {
			return { items: [] }
		}
		default:
			return cart
	}
}
export { ACTIONS, reducer, init, initialState }

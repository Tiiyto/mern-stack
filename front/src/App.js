import { useEffect, useMemo, useReducer } from 'react'
import { Route, Switch } from 'react-router-dom'
import './App.css'
import NavBar from './components/navbar'
import PrivateRoute from './privateRoute'
import AdminRoute from './adminRoute'
import UnloggedRoute from './unloggedRoute'
import Home from './pages/home'
import Login from './pages/authentication/login'
import Error404 from './pages/error404'
import Register from './pages/authentication/register'
import NewArticle from './pages/admin/newArticle'
import Profile from './pages/user/profile'
import Picture from './pages/user/uploadPicture'
import ECommerce from './pages/ecommerce/ecommerce'
import ProductPage from './pages/ecommerce/product'
import ResetPassword from './pages/authentication/resetPassword'
import ResetPasswordNew from './pages/authentication/resetNewPassword'
import Cart from './pages/ecommerce/cart'
import EditProfile from './components/user/editProfile'
import Checkout from './pages/ecommerce/checkout'
import Order from './pages/ecommerce/order'
import MainAdmin from './pages/admin/mainAdmin'
import NewProduct from './pages/admin/newProduct'
import { UserContext, CartContext } from './Context'
import {
	reducer as cartReducer,
	init as initCart,
	initialState as initialStateCart,
} from './reducer/cart'
import {
	reducer as authReducer,
	init as initAuth,
	initialState as initialStateAuth,
} from './reducer/user'
import EditProduct from './pages/admin/editProduct'

const App = () => {
	const [auth, dispatchUser] = useReducer(authReducer, initialStateAuth, initAuth)
	const providerUser = useMemo(() => ({ auth, dispatch: dispatchUser }), [auth, dispatchUser])

	const [cart, dispatch] = useReducer(cartReducer, initialStateCart, initCart)
	const providerCart = useMemo(() => ({ cart, dispatch }), [cart, dispatch])

	useEffect(() => {
		if (cart.items.length === 0) localStorage.removeItem('cart')
		else localStorage.setItem('cart', JSON.stringify(cart))
	}, [cart])

	return (
		<div className="App">
			<UserContext.Provider value={providerUser}>
				<CartContext.Provider value={providerCart}>
					<NavBar />
					<Switch>
						<Route exact path="/" component={Home} />

						{/* Auth Pages */}
						<UnloggedRoute exact path="/register" component={Register} />
						<UnloggedRoute exact path="/login" component={Login} />
						<UnloggedRoute exact path="/reset-password" component={ResetPassword} />
						<UnloggedRoute path="/reset-password/:token" component={ResetPasswordNew} />

						{/* User Pages */}
						<PrivateRoute exact path="/profile" component={Profile} />
						<PrivateRoute exact path="/profile/orders" component={Order} />
						<PrivateRoute exact path="/profile/edit" component={EditProfile} />
						<PrivateRoute exact path="/profile/edit/picture" component={Picture} />

						{/* Ecommerce Pages */}
						<Route exact path="/e-commerce" component={ECommerce} />
						<Route path="/e-commerce/:slug" component={ProductPage} />
						<Route exact path="/cart" component={Cart} />
						<Route exact path="/checkout" component={Checkout} />

						{/* Admin */}
						<AdminRoute exact path="/admin" component={MainAdmin} />

						<AdminRoute exact path="/admin/product/new" component={NewProduct} />
						<AdminRoute path="/admin/product/edit/:id" component={EditProduct} />

						<AdminRoute exact path="/admin/article/new" component={NewArticle} />
						<AdminRoute path="/admin/article/edit/:id" component={NewArticle} />

						<AdminRoute exact path="/admin/article/new" component={NewArticle} />
						<AdminRoute path="/admin/article/edit/:id" component={NewArticle} />

						{/* Wrong Page */}
						<Route path="*" component={Error404} />
					</Switch>
				</CartContext.Provider>
			</UserContext.Provider>
		</div>
	)
}

export default App

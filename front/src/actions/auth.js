import AuthAPI from '../api/auth'
import UserAPI from '../api/user'
import { ACTIONS } from '../reducer/user'

const TOKEN_KEY = 'auth-token'

const signIn = async (dispatch, payload) => {
	dispatch({ type: ACTIONS.LOADING })
	const response = await AuthAPI.login(payload.username, payload.password)
	if (response) {
		if (response.status === 200) {
			dispatch({ type: ACTIONS.LOGGED, payload: response.user })
		} else {
			console.error(response.data)
			dispatch({ type: ACTIONS.LOGGED_ERROR })
		}
	}
}

const signInGoogle = (dispatch, payload, history) => {
	localStorage.setItem('tokens', JSON.stringify(payload.tokenObj))
	localStorage.setItem('user', JSON.stringify(payload.profileObj))
	dispatch({ type: ACTIONS.LOGGED, payload: payload.profileObj })
	history.push('/profile')
}

const logout = (dispatch, history) => {
	if (localStorage.getItem('tokens')) localStorage.removeItem('tokens')
	else {
		AuthAPI.logout()
		localStorage.removeItem(TOKEN_KEY)
	}
	history.push('/')
	dispatch({ type: ACTIONS.LOGOUT })
}

const refreshUser = async (dispatch, history) => {
	try {
		const user = await UserAPI.data()
		dispatch({ type: ACTIONS.CHANGE, payload: user })
	} catch (err) {
		history.push('/')
		dispatch({ type: ACTIONS.LOGOUT })
	}
}

export { signIn, signInGoogle, logout, refreshUser }

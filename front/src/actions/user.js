import UserAPI from '../api/user'
import { ACTIONS } from '../reducer/user'

const refreshUser = async (dispatch, history) => {
	try {
		const user = await UserAPI.data()
		dispatch({ type: ACTIONS.LOGGED, payload: user })
	} catch (err) {
		history.push('/')
		dispatch({ type: ACTIONS.LOGOUT })
	}
}

const editUser = async (dispatch) => {
	try {
		const user = await UserAPI.editUser()
		dispatch({ type: ACTIONS.LOGGED, payload: user })
	} catch (err) {}
}

export { refreshUser, editUser }

import ProductAPI from '../api/product'
import { ACTIONS } from '../reducer/product'

const getProducts = async (dispatch, { page, limit = 5 }, newPage = false) => {
	try {
		if (newPage) {
			window.history.pushState(null, '', `/e-commerce?page=${page}`)
			window.scrollTo({ top: 0, behavior: 'smooth' })
		}
		dispatch({ type: ACTIONS.FETCH_DATA_REQUEST, payload: page })
		const data = await ProductAPI.getAllProduct(page, limit)
		dispatch({ type: ACTIONS.FETCH_DATA_RESPONSE, payload: data })
	} catch (err) {
		dispatch({ type: ACTIONS.ERROR })
	}
}

export { getProducts }

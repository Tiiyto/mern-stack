import axios from 'axios'
import authHeader from './authHeader'

const BASE_URL = process.env.REACT_APP_API_URL
	? `${process.env.REACT_APP_API_URL}/api/article`
	: `/api/article`

class ArticleAPI {
	async getAllArticles() {
		const response = await axios.get(`${BASE_URL}/`, {
			headers: authHeader(),
		})
		return response.data
	}
}

export default new ArticleAPI()

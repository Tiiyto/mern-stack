import axios from 'axios'
import jwt_decode from 'jwt-decode'

const API_URL = process.env.REACT_APP_API_URL ? `${process.env.REACT_APP_API_URL}` : ``
const TOKEN_KEY = 'auth-token'
const BASE_URL = `${API_URL}/api/auth`

axios.interceptors.response.use(
	(response) => {
		return response
	},
	async (error) => {
		const originalRequest = error.config
		if (!error.response) return Promise.reject(error)
		if (!originalRequest._retry && error.response.status === 401) {
			originalRequest._retry = true
			const response = await axios.get(`${BASE_URL}/token`, { withCredentials: true })
			if (response.status === 200) {
				const accessToken = response.data
				localStorage.setItem(TOKEN_KEY, accessToken)
				originalRequest.headers.Authorization = `Bearer ${accessToken}`
				return axios(originalRequest)
			}
		}
		return Promise.reject(error)
	}
)

class AuthAPI {
	/**
	 *
	 * @param {String} username
	 * @param {String} password
	 * @return response
	 */
	async login(username, password) {
		try {
			const response = await axios.post(
				`${BASE_URL}/cookies/login`,
				{ username, password },
				{
					withCredentials: true,
				}
			)
			const token = response.data
			localStorage.setItem(TOKEN_KEY, token)
			const user = jwt_decode(token).user
			if (user.imageUrl) user.imageUrl = `${API_URL}/api/public/avatars/${user.imageUrl}`
			response.user = user
			return response
		} catch (err) {
			return err
		}
	}

	async logout() {
		await axios.delete(`${BASE_URL}/cookies/logout`, {
			withCredentials: true,
		})
	}

	async resetPassword(emailAddress) {
		try {
			const response = await axios.post(`${BASE_URL}/reset-password`, {
				emailAddress: emailAddress,
			})
			return response.data
		} catch (err) {
			throw new Error(err.response.data)
		}
	}

	async resetPasswordDone(resetPasswordToken, newPassword) {
		try {
			await axios.post(`${BASE_URL}/reset-password-done`, {
				refreshPasswordToken: resetPasswordToken,
				newPassword: newPassword,
			})
		} catch (err) {
			throw new Error(err.response.data)
		}
	}

	/**
	 *
	 * @returns User
	 */
	getCurrentUser() {
		const token = JSON.parse(localStorage.getItem(TOKEN_KEY))
		const user = jwt_decode(token).user
		if (user.imageUrl) user.imageUrl = `${API_URL}/api/public/avatars/${user.imageUrl}`
		if (user) return user
		else return null
	}
}

export default new AuthAPI()

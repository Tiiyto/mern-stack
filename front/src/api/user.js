import axios from 'axios'
import authHeader from './authHeader'

const API_URL = process.env.REACT_APP_API_URL ? `${process.env.REACT_APP_API_URL}` : ``
const BASE_URL = `${API_URL}/api/user`

class UserAPI {
	async data() {
		const response = await axios.get(`${BASE_URL}/data`, {
			headers: authHeader(),
		})
		const user = response.data
		if (user.imageUrl) user.imageUrl = `${API_URL}/api/public/avatars/${user.imageUrl}`
		return user
	}

	async picture(data) {
		const response = await axios.post(`${BASE_URL}/picture`, data, {
			headers: authHeader(),
		})
		const user = response.data.user
		user.imageUrl = `${API_URL}/api/public/avatars/${user.imageUrl}`
		localStorage.setItem('user', JSON.stringify(user))
		return response.data
	}

	/**
	 *
	 * @param {String} username
	 * @param {String} password
	 * @param {String} firstName
	 * @param {String} lastName
	 * @param {String} emailAddress
	 */
	async register(username, password, firstName, lastName, emailAddress) {
		try {
			const response = await axios.post(`${BASE_URL}/register`, {
				username,
				password,
				firstName,
				lastName,
				emailAddress,
			})
			return response
		} catch (err) {
			return err.response
		}
	}
}

export default new UserAPI()

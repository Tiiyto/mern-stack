const TOKEN_KEY = 'auth-token'

export default function authHeader() {
	const accessToken = localStorage.getItem(TOKEN_KEY)
	if (accessToken) {
		return { Authorization: 'Bearer ' + accessToken }
	} else {
		return {}
	}
}

import axios from 'axios'
import authHeader from './authHeader'

const BASE_URL = process.env.REACT_APP_API_URL
	? `${process.env.REACT_APP_API_URL}/api/product`
	: `/api/product`

class ProductAPI {
	async getAllProduct(page, limit) {
		const response = await axios.get(BASE_URL, {
			params: { page: page, limit: limit },
		})
		return response.data
	}
	async getProductBySlug(slug) {
		const response = await axios.get(`${BASE_URL}/slug/${slug}`)
		return response.data
	}

	async getProductByID(id) {
		const response = await axios.get(`${BASE_URL}/${id}`)
		return response.data
	}

	async paid(amount, source, receipt_email) {
		const response = await axios.post(
			`${BASE_URL}/charge`,
			{
				amount,
				source,
				receipt_email,
			},
			{
				headers: authHeader(),
			}
		)
		return response.data
	}

	async create(title, price, description, quantity, pictureURL) {
		const response = await axios.post(
			BASE_URL,
			{
				title,
				price,
				description,
				quantity,
				picture: pictureURL,
			},
			{
				headers: authHeader(),
			}
		)
		return response
	}

	async deleteMany(ids) {
		const response = await axios.delete(BASE_URL, { headers: authHeader(), data: { ids: ids } })
		return response
	}

	async update(product, id) {
		const { title, price, description, quantity, pictureURL } = product
		const response = await axios.put(
			`${BASE_URL}/${id}`,
			{
				title,
				price,
				description,
				quantity,
				picture: pictureURL,
			},
			{
				headers: authHeader(),
			}
		)
		return response
	}
}

export default new ProductAPI()

import axios from 'axios'
import authHeader from './authHeader'

const BASE_URL = process.env.REACT_APP_API_URL
	? `${process.env.REACT_APP_API_URL}/api/order`
	: `/api/order`

class orderAPI {
	async getOrders() {
		const response = await axios.get(`${BASE_URL}`, {
			headers: authHeader(),
		})
		return response.data
	}
	async paid(amount, cart, source, receipt_email) {
		const response = await axios.post(
			`${BASE_URL}/charge`,
			{
				cart: cart.items,
				amount,
				source,
				receipt_email,
			},
			{
				headers: authHeader(),
			}
		)
		return response.data
	}
}

export default new orderAPI()

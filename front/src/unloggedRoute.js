import { useContext } from 'react'
import { Route, Redirect } from 'react-router-dom'
import { UserContext } from './Context'

const UnloggedRoute = ({ component: Component, path, ...props }) => {
	const { auth } = useContext(UserContext)
	if (auth.user) {
		return <Redirect to="/profile" />
	}
	return <Route path={path} component={Component} {...props} />
}

export default UnloggedRoute

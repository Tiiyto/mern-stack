import { useContext } from 'react'
import { Route, Redirect } from 'react-router-dom'
import { UserContext } from './Context'

const PrivateRoute = ({ component: Component, path, ...props }) => {
	const { auth } = useContext(UserContext)
	if (!auth.user) {
		return <Redirect to="/login" />
	}
	return <Route path={path} component={Component} {...props} />
}

export default PrivateRoute

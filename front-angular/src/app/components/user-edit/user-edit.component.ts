import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { DomSanitizer } from '@angular/platform-browser';
import { AuthService } from 'src/app/services/auth.service';
import { UserService } from 'src/app/services/user.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-user-edit',
  templateUrl: './user-edit.component.html',
  styleUrls: ['./user-edit.component.scss'],
})
export class UserEditComponent implements OnInit {
  form: FormGroup;
  avatarUrl: string;
  picture: File;

  constructor(
    private fb: FormBuilder,
    private AuthService: AuthService,
    private userService: UserService,
    private dom: DomSanitizer,
    private dialogRef: MatDialogRef<UserEditComponent>
  ) {
    this.form = this.fb.group({
      username: ['', Validators.required],
      email: ['', Validators.required],
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
    });
  }

  ngOnInit(): void {
    this.userService.getData().subscribe((userData) => {
      this.form.setValue({
        username: userData.username,
        email: userData.email,
        firstName: userData.firstName,
        lastName: userData.lastName,
      });
      this.avatarUrl = `${environment.apiUrl}/public/avatars/${userData?.imageUrl}`;
    });
  }

  handleFileInput(event: Event) {
    const input = event.target as any;
    const file: File = (input.files as any)[0];
    this.picture = file;
    const pictureURL: string = URL.createObjectURL(file);
    this.avatarUrl = this.dom.bypassSecurityTrustUrl(pictureURL) as string;
  }

  save(): void {
    this.userService
      .edit(this.picture, this.form.value)
      .subscribe((accessToken) => {
        this.AuthService.setUser(accessToken);
        this.dialogRef.close();
      });
  }
}

import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  form: FormGroup;
  loading = false;
  error = false;
  hide = true;

  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private router: Router
  ) {
    this.form = this.fb.group({
      username: ['', Validators.required],
      password: ['', Validators.required],
    });
  }

  ngOnInit(): void {}

  login(): void {
    if (this.form.valid) {
      const { username, password } = this.form.value;
      this.loading = true;
      this.authService.login(username, password).subscribe(
        () => {
          this.loading = false;
          this.router.navigate(['/product']);
        },
        (err) => {
          this.loading = false;
          this.error = true;
        }
      );
    }
  }

  register(): void {
    this.router.navigate(['/register']);
  }
}

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';
import { Product } from 'src/app/models/product';
import { ProductService } from 'src/app/services/product.service';
import { ProductCardComponent } from '../product-card/product-card.component';
import { ProductListComponent } from './product-list.component';
import { MatCardModule } from '@angular/material/card';
import { MatInputModule } from '@angular/material/input';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';
import { MatIconModule } from '@angular/material/icon';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Metadata } from 'src/app/models/metadata';
import { RouterTestingModule } from '@angular/router/testing';
import { ShortenPipe } from 'src/app/pipes/shorten.pipe';
import { PaginationComponent } from '../../pagination/pagination.component';

describe('ProductListComponent', () => {
  let fixture: ComponentFixture<ProductListComponent>;
  let mockActivatedRoute: any, mockLocation: any, mockProductService: any;

  let PRODUCTS: Product[];
  let METADATA: Metadata;

  beforeEach(() => {
    PRODUCTS = [
      {
        pictures: [
          'https://upload.wikimedia.org/wikipedia/commons/thumb/b/b6/Image_created_with_a_mobile_phone.png/250px-Image_created_with_a_mobile_phone.png',
        ],
        _id: '1',
        title: 'Test titre 1',
        price: 15,
        description: 'Ceci est une description 1',
        quantity: 50,
        slug: 'Test-titre-1',
      },
      {
        pictures: [
          'https://upload.wikimedia.org/wikipedia/commons/thumb/b/b6/Image_created_with_a_mobile_phone.png/250px-Image_created_with_a_mobile_phone.png',
        ],
        _id: '2',
        title: 'Test titre 2',
        price: 544,
        description: 'Ceci est une description 2',
        quantity: 50,
        slug: 'Test-titre-2',
      },
      {
        pictures: [
          'https://upload.wikimedia.org/wikipedia/commons/thumb/b/b6/Image_created_with_a_mobile_phone.png/250px-Image_created_with_a_mobile_phone.png',
        ],
        _id: '3',
        title: 'Test titre 3',
        price: 33,
        description: 'Ceci est une description 3',
        quantity: 345,
        slug: 'Test-titre-3',
      },
    ];
    METADATA = {
      page: 1,
      per_page: 5,
      page_count: 2,
      total_count: 10,
    };
    mockActivatedRoute = {
      queryParams: of({ page: 1 }),
    };
    mockProductService = jasmine.createSpyObj(['getProducts']);
    mockLocation = jasmine.createSpyObj(['replaceState']);
    TestBed.configureTestingModule({
      declarations: [
        ProductListComponent,
        ProductCardComponent,
        ShortenPipe,
        PaginationComponent,
      ],
      imports: [
        MatCardModule,
        MatInputModule,
        NgxSkeletonLoaderModule,
        MatIconModule,
        BrowserAnimationsModule,
        RouterTestingModule,
      ],
      providers: [
        { provide: ProductService, useValue: mockProductService },
        { provide: ActivatedRoute, useValue: mockActivatedRoute },
        { provide: Location, useValue: mockLocation },
      ],
    });
    fixture = TestBed.createComponent(ProductListComponent);
  });

  it('should render each product as a product card', () => {
    mockProductService.getProducts.and.returnValue(
      of({ products: PRODUCTS, _metadata: METADATA })
    );

    //run ngOnInit
    fixture.detectChanges();

    const productComponentsDEs = fixture.debugElement.queryAll(
      By.directive(ProductCardComponent)
    );
    expect(productComponentsDEs.length).toEqual(3);
    for (let i = 0; i < productComponentsDEs.length; i++) {
      expect(productComponentsDEs[i].componentInstance.product).toEqual(
        PRODUCTS[i]
      );
    }
  });
});

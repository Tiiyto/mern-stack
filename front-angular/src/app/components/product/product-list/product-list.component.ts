import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subject } from 'rxjs';
import { Product } from '../../../models/product';
import { ProductService } from '../../../services/product.service';
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { Metadata } from 'src/app/models/metadata';
import { Location } from '@angular/common';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.scss'],
})
export class ProductListComponent implements OnInit {
  products: Product[] = [];
  metadata: Metadata;
  loading: boolean = false;
  private searchTerms = new Subject<string>();

  constructor(
    private productService: ProductService,
    private route: ActivatedRoute,
    private location: Location
  ) {}

  ngOnInit(): void {
    this.route.queryParams.subscribe((queryParams) => {
      const page = queryParams.page ? Number(queryParams.page) : 1;
      this.getProducts(page);
      window.scrollTo({ top: 0, behavior: 'smooth' });
    });

    this.searchTerms
      .pipe(
        debounceTime(500),
        distinctUntilChanged(),
        switchMap((term: string) => {
          this.loading = true;
          return this.productService.searchProduct(term);
        })
      )
      .subscribe(
        (productData) => {
          this.location.replaceState('/product');
          this.products = productData.products;
          this.metadata = productData._metadata;
          this.loading = false;
        },
        (err) => {
          this.loading = false;
          console.error(err);
        }
      );
  }

  getProducts(page: number): void {
    this.loading = true;
    this.productService.getProducts(page, 15).subscribe((productData) => {
      this.products = productData.products;
      this.metadata = productData._metadata;
      this.loading = false;
    });
  }

  searchProduct(term: string): void {
    this.searchTerms.next(term);
  }
}

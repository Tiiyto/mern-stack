import { NgModule } from '@angular/core';
import { MatCardModule } from '@angular/material/card';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { RouterModule } from '@angular/router';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';
import { ShortenPipe } from 'src/app/pipes/shorten.pipe';
import { SharedModule } from 'src/app/shared.module';
import { PaginationComponent } from '../pagination/pagination.component';
import { ProductCardSkeletonComponent } from './product-card-skeleton/product-card-skeleton.component';
import { ProductCardComponent } from './product-card/product-card.component';
import { ProductDetailComponent } from './product-detail/product-detail.component';
import { ProductListComponent } from './product-list/product-list.component';
import { ProductResolver } from './product-resolver.service';

@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild([
      { path: 'product', component: ProductListComponent},
      { 
        path: 'product/:slug', 
        component: ProductDetailComponent,
        resolve: {resolvedData: ProductResolver}
      }
    ]),
    MatCardModule,
    MatInputModule,
    NgxSkeletonLoaderModule,
    MatIconModule
  ],
  declarations: [
    ProductCardComponent,
    ProductListComponent,
    ProductDetailComponent,
    PaginationComponent,
    ProductCardSkeletonComponent,
    ShortenPipe,
  ],
})
export class ProductModule { }

import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { Product, ProductResolved } from '../../../models/product';

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.scss'],
})
export class ProductDetailComponent implements OnInit {
  product: Product;
  errorMessage: string;

  constructor( private route: ActivatedRoute, private titleService: Title, private router: Router) {}

  ngOnInit(): void {
    const resolvedData: ProductResolved = this.route.snapshot.data['resolvedData'];
    if(resolvedData.error != undefined ) this.errorMessage = resolvedData.error;
    this.onProductRetrieved(resolvedData.product);
  }

  onProductRetrieved(product: Product | null): void {
    this.product = product!;
    if (this.product) {
      this.titleService.setTitle(`Product Detail: ${this.product.title}`);
    } else {
      this.router.navigateByUrl('404-not-found');  
    }
  }
}

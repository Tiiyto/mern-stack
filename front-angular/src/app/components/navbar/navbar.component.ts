import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from 'src/app/models/user';
import { AuthService } from 'src/app/services/auth.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss'],
})
export class NavbarComponent implements OnInit {
  user: User | null;
  avatarUrl: string;
  constructor(private AuthService: AuthService, private router: Router) {}

  ngOnInit(): void {
    this.AuthService.user$.subscribe((user) => {
      this.user = user;
      if(user?.imageUrl === 'undefined') this.avatarUrl = 'https://picsum.photos/200';
      else this.avatarUrl = `${environment.apiUrl}/public/avatars/${user?.imageUrl}`; 
    });
  }

  logout(): void {
    this.AuthService.logout();
  }
}

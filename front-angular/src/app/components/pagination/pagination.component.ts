import { Component, Input, OnInit, SimpleChanges } from '@angular/core';
import { Metadata } from 'src/app/models/metadata';

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.scss'],
})
export class PaginationComponent implements OnInit {
  @Input()
  metadata: Metadata;
  @Input()
  model: string;

  paginationTab: number[] = [];
  constructor() {}

  ngOnInit(): void {}
  ngOnChanges(changes: SimpleChanges): void {
    this.paginationTab = [];
    for (let i = 1; i < this.metadata.page_count + 1; i++) {
      this.paginationTab.push(i);
    }
  }
}

import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UserService } from 'src/app/services/user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
})
export class RegisterComponent implements OnInit {
  form: FormGroup;

  constructor(
    private fb: FormBuilder,
    private UserService: UserService,
    private router: Router
  ) {
    this.form = this.fb.group({
      username: ['', Validators.required],
      password: '',
      firstName: '',
      lastName: '',
      emailAddress: '',
    });
  }

  ngOnInit(): void {}

  register() {
    if (this.form.valid) {
      const { username, password, firstName, lastName, emailAddress } =
        this.form.value;
      this.UserService.register(
        username,
        password,
        firstName,
        lastName,
        emailAddress
      ).subscribe(
        () => {
          this.router.navigate(['/login']);
        },
        (err) => console.log(err)
      );
    }
  }
}

import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';

@Component({
  template: `
    <h1>This is not the page you were looking for!</h1>
    `
})
export class PageNotFoundComponent implements OnInit {

  constructor( private titleService: Title ) { }

  ngOnInit(): void {
    this.titleService.setTitle('Page Not Found');
  }

}

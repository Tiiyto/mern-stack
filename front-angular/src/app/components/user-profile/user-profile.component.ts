import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { UserEditComponent } from '../user-edit/user-edit.component';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.scss'],
})
export class UserProfileComponent implements OnInit {
  constructor(public dialog: MatDialog) {}

  ngOnInit(): void {}

  editProfile(): void {
    this.dialog.open(UserEditComponent, {
      width: '50vw',
      maxHeight: '90vh',
    });
  }

  closeDialog(): void {
    this.dialog.closeAll();
  }
}

import { HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ResponseWithTime } from '../models/response';

@Injectable({
  providedIn: 'root',
})
export class HttpCacheService {
  private requests: any = {};

  put(url: string, response: HttpResponse<any>): void {
    const date = new Date();
    this.requests[url] = { response: response, date: date };
  }

  get(url: string): ResponseWithTime | undefined {
    return this.requests[url];
  }

  invalidateUrl(url: string): void {
    this.requests[url] = undefined;
  }

  invalidateCache(): void {
    this.requests = {};
  }
}

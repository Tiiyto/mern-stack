import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { UserData } from '../models/user';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  constructor(private http: HttpClient) {}

  edit(picture: File, userData: UserData): Observable<string> {
    const formData: FormData = new FormData();
    if (picture) formData.append('file', picture);

    formData.append('username', userData.username);
    formData.append('firstName', userData.firstName);
    formData.append('lastName', userData.lastName);
    formData.append('emailAddress', userData.email);
    formData.append('address', JSON.stringify(userData.address));

    return this.http.put<string>(`${environment.apiUrl}/user/edit`, formData);
  }

  register(
    username: string,
    password: string,
    firstName: string,
    lastName: string,
    emailAddress: string
  ): Observable<string> {
    return this.http.post(
      `${environment.apiUrl}/user/register`,
      {
        username,
        password,
        firstName,
        lastName,
        emailAddress,
      },
      { responseType: 'text' }
    );
  }

  getData(): Observable<UserData> {
    return this.http.get<UserData>(`${environment.apiUrl}/user/data`);
  }
}

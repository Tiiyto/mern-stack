import { TestBed } from '@angular/core/testing';
import { ProductService } from './product.service';
import {
  HttpClientTestingModule,
  HttpTestingController,
  TestRequest,
} from '@angular/common/http/testing';
import { ProductData } from '../models/product';
import { environment } from 'src/environments/environment';

describe('ProductService', () => {
  let productService: ProductService;
  let httpTestingController: HttpTestingController;
  let PRODUCTDATA: ProductData;

  beforeEach(() => {
    PRODUCTDATA = {
      products: [
        {
          pictures: [
            'https://upload.wikimedia.org/wikipedia/commons/thumb/b/b6/Image_created_with_a_mobile_phone.png/250px-Image_created_with_a_mobile_phone.png',
          ],
          _id: '1',
          title: 'Test titre 1',
          price: 15,
          description: 'Ceci est une description 1',
          quantity: 50,
          slug: 'Test-titre-1',
        },
        {
          pictures: [
            'https://upload.wikimedia.org/wikipedia/commons/thumb/b/b6/Image_created_with_a_mobile_phone.png/250px-Image_created_with_a_mobile_phone.png',
          ],
          _id: '2',
          title: 'Test titre 2',
          price: 544,
          description: 'Ceci est une description 2',
          quantity: 50,
          slug: 'Test-titre-2',
        },
        {
          pictures: [
            'https://upload.wikimedia.org/wikipedia/commons/thumb/b/b6/Image_created_with_a_mobile_phone.png/250px-Image_created_with_a_mobile_phone.png',
          ],
          _id: '3',
          title: 'Test titre 3',
          price: 33,
          description: 'Ceci est une description 3',
          quantity: 345,
          slug: 'Test-titre-3',
        },
      ],
      _metadata: {
        page: 1,
        per_page: 5,
        page_count: 2,
        total_count: 10,
      },
    };
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [ProductService],
    });
    productService = TestBed.inject(ProductService);
    httpTestingController = TestBed.inject(HttpTestingController);
  });

  afterEach(() => {
    httpTestingController.verify();
  });

  it('should GET all products from page 1', () => {
    productService.getProducts(1).subscribe((data: ProductData) => {
      expect(data.products.length).toBe(3);
    });
    let productDataRequest: TestRequest = httpTestingController.expectOne(
      `${environment.apiUrl}/product?page=1&&limit=5`
    );
    expect(productDataRequest.request.method).toEqual('GET');

    productDataRequest.flush(PRODUCTDATA);
  });
});

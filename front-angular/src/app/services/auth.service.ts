import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { Token, User } from '../models/user';
import { environment } from 'src/environments/environment';
import { BehaviorSubject, Observable } from 'rxjs';
import jwt_decode from 'jwt-decode';

const TOKEN_KEY = 'auth-token';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  private authUrl = `${environment.apiUrl}/auth`; // URL to web api

  accessToken: string | null;
  private _user: BehaviorSubject<User | null>;
  user$: Observable<User | null>;

  constructor(private http: HttpClient) {
    this.accessToken = JSON.parse(localStorage.getItem(TOKEN_KEY)!);
    const user = this.accessToken
      ? jwt_decode<Token>(this.accessToken).user
      : null;
    this._user = new BehaviorSubject<User | null>(user);
    this.user$ = this._user.asObservable();
  }

  login(username: string, password: string): Observable<string> {
    return this.http
      .post<string>(
        `${this.authUrl}/cookies/login`,
        {
          username,
          password,
        },
        { withCredentials: true }
      )
      .pipe(
        map((accessToken) => {
          this.setUser(accessToken);
          return accessToken;
        })
      );
  }

  setUser(accessToken: string): void {
    localStorage.setItem(TOKEN_KEY, JSON.stringify(accessToken));
    this.accessToken = accessToken;
    const user: User = jwt_decode<Token>(accessToken).user;
    this._user.next(user);
  }

  isLoggedIn(): boolean {
    if (this.accessToken) return true;
    return false;
  }

  logout(): void {
    localStorage.removeItem(TOKEN_KEY);
    this.accessToken = null;
    this._user.next(null);
    this.http
      .delete(`${this.authUrl}/cookies/logout`, {
        withCredentials: true,
        responseType: 'text',
      })
      .subscribe();
  }

  refreshToken(): Observable<string> {
    return this.http.get<string>(`${this.authUrl}/token`, {
      withCredentials: true,
    });
  }
  isTokenExpired(): boolean {
    if (this.accessToken) {
      const exp = jwt_decode<Token>(this.accessToken).exp;
      const expDate = new Date(
        new Date('1970,1,1').getUTCMilliseconds() + exp * 1000
      );
      if (expDate < new Date()) return true;
      return false;
    }
    throw new Error('No token');
  }
}

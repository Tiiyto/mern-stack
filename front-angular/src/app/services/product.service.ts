import { HttpClient, HttpContext } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Product, ProductData } from '../models/product';
import { environment } from 'src/environments/environment';
import { CACHEABLE, CACHETIME } from '../helpers/cache.interceptor';

@Injectable({
  providedIn: 'root',
})
export class ProductService {
  private productUrl = `${environment.apiUrl}/product`; // URL to web api

  constructor(private http: HttpClient) {}

  getProducts(page: number, limit: number = 5): Observable<ProductData> {
    const url = `${this.productUrl}?page=${page}&&limit=${limit}`;
    return this.http.get<ProductData>(url, {
      context: new HttpContext().set(CACHEABLE, true),
    });
  }
  getProductBySlug(slug: string): Observable<Product> {
    const url = `${this.productUrl}/slug/${slug}`;
    return this.http.get<Product>(url, {
      context: new HttpContext().set(CACHEABLE, true).set(CACHETIME, 2),
    });
  }

  searchProduct(searchTerm: string): Observable<ProductData> {
    if (!searchTerm.trim()) return this.getProducts(1);
    const url = `${this.productUrl}/search?searchTerm=${searchTerm}`;
    return this.http.get<ProductData>(url);
  }
}

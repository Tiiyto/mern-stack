import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { AuthService } from '../services/auth.service';
import { environment } from 'src/environments/environment';
import { catchError, switchMap } from 'rxjs/operators';

@Injectable()
export class JwtInterceptor implements HttpInterceptor {
  isRefreshed = false;
  constructor(private AuthService: AuthService) {}

  intercept(
    request: HttpRequest<unknown>,
    next: HttpHandler
  ): Observable<HttpEvent<unknown>> {
    const accessToken = this.AuthService.accessToken;
    const isApiUrl = request.url.startsWith(environment.apiUrl);
    if (isApiUrl && accessToken) {
      if (this.AuthService.isTokenExpired() === true && !this.isRefreshed) {
        this.isRefreshed = true;
        return this.AuthService.refreshToken().pipe(
          switchMap((token) => {
            this.AuthService.setUser(token);
            this.isRefreshed = false;
            return next.handle(this.addTokenHeader(request, token));
          }),
          catchError((err) => {
            this.AuthService.logout();
            return throwError(err);
          })
        );
      } else {
        request = this.addTokenHeader(request, accessToken);
        return next.handle(request);
      }
    } else {
      return next.handle(request);
    }
  }

  private addTokenHeader(
    request: HttpRequest<any>,
    accessToken: string
  ): HttpRequest<any> {
    return request.clone({
      setHeaders: {
        Authorization: `Bearer ${accessToken}`,
      },
    });
  }
}

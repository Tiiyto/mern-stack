import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpContextToken,
  HttpResponse,
  HTTP_INTERCEPTORS,
} from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { HttpCacheService } from '../services/http-cache.service';
import { tap } from 'rxjs/operators';
import { ResponseWithTime } from '../models/response';

export const CACHEABLE = new HttpContextToken(() => false);
export const CACHETIME = new HttpContextToken(() => 1);

@Injectable()
export class CacheInterceptor implements HttpInterceptor {
  constructor(private cacheService: HttpCacheService) {}

  intercept(
    req: HttpRequest<unknown>,
    next: HttpHandler
  ): Observable<HttpEvent<unknown>> {
    //only cache requests configured to be cacheable
    if (!req.context.get(CACHEABLE)) return next.handle(req);

    //pass along non-cacheable requests and invalidate cache
    if (req.method !== 'GET') {
      this.cacheService.invalidateCache();
      return next.handle(req);
    }
    //attempt to retrieve a cached response
    const cachedResponse: ResponseWithTime | undefined = this.cacheService.get(
      req.url
    );
    // return cached response
    if (cachedResponse) {
      const diff: number = Math.abs(
        cachedResponse.date.getTime() - new Date().getTime()
      );
      const minutes = Math.floor(diff / 1000 / 60);
      if (minutes < req.context.get(CACHETIME)) {
        return of(cachedResponse.response);
      }
    }
    // send request to server and add response to cache
    return next.handle(req).pipe(
      tap((event) => {
        if (event instanceof HttpResponse) {
          this.cacheService.put(req.url, event);
        }
      })
    );
  }
}

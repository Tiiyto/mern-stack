export interface Metadata {
  page: number;
  per_page: number;
  page_count: number;
  total_count: number;
}

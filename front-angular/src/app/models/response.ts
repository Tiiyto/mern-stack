import { HttpResponse } from '@angular/common/http';

export interface ResponseWithTime {
  response: HttpResponse<any>;
  date: Date;
}

import { Metadata } from './metadata';

export interface Product {
  _id: string;
  title: string;
  slug: string;
  price: number;
  pictures: [string];
  description: string;
  quantity: number;
}

export interface ProductData {
  _metadata: Metadata;
  products: Product[];
}

export interface ProductResolved {
  product: Product | null;
  error?: any;
}

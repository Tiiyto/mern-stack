export interface User {
  ID: string;
  username: string;
  firstName: string;
  lastName: string;
  imageUrl: string;
  isAdmin: boolean;
}

export interface Token {
  user: User;
  iat: number;
  exp: number;
}

export interface UserData {
  ID: string;
  username: string;
  email: string;
  firstName: string;
  lastName: string;
  imageUrl: string;
  address: { country: string; address: string; postCode: string };
}

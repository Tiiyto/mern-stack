import { ShortenPipe } from './shorten.pipe';

describe('ShortenPipe', () => {
  it('should change the string', () => {
    const pipe = new ShortenPipe();
    const string = 'Test of the shorten Pipe'
    const limit = 4;
    expect(pipe.transform(string,limit)).toEqual('Test [...]');
  });

  it('should not change the string', () => {
    const pipe = new ShortenPipe();
    const string = 'Test of the shorten Pipe'
    const limit = 200;

    expect(pipe.transform(string,limit)).toEqual('Test of the shorten Pipe');
  })
});

const bcrypt = require('bcrypt')
require('dotenv').config()
const jwt = require('jsonwebtoken')
const User = require('../models/User')

/**
 *
 * @param {User} user
 * @returns token
 */
const generateAccessToken = (user) => {
	return jwt.sign(
		{
			user: {
				id: user._id,
				username: user.username,
				firstName: user.firstName,
				lastName: user.lastName,
				emailAddress: user.emailAddress,
			},
		},
		process.env.ACCESS_TOKEN_SECRET,
		{ expiresIn: '10min' }
	)
}

/**
 * Create a user if the username is not in the database
 * @param {String} username
 * @param {String} password
 * @param {String} firstName
 * @param {String} lastName
 * @param {String} emailAddress
 * @returns user
 */
const createUser = async (username, password, firstName, lastName, emailAddress) => {
	try {
		const existingUser = await User.findOne({ username: username })
		if (existingUser) throw new Error('A user exists.')
		const passwordHashed = await bcrypt.hash(password, 10)
		const newUser = new User({
			username: username,
			password: passwordHashed,
			firstName: firstName,
			lastName: lastName,
			emailAddress: emailAddress,
		})
		await newUser.save()
		return newUser
	} catch (err) {
		throw err
	}
}

/**
 *
 * @param {String} username
 * @param {String} password
 * @returns data
 */
const logUser = async (username, password) => {
	try {
		const user = await User.findOne({ username: username })
		if (bcrypt.compareSync(password, user.password)) {
			const accessToken = generateAccessToken(user)
			const refreshToken = jwt.sign({ userID: user._id }, process.env.REFRESH_TOKEN_SECRET)
			user.tokens.refreshToken.push(refreshToken)
			await user.save()
			return {
				user: {
					email: user.emailAddress,
					familyName: user.lastName,
					givenName: user.firstName,
					ID: user._id,
					imageUrl: user.picture,
					name: `${user.firstName} ${user.lastName}`,
				},
				tokens: {
					refreshToken: refreshToken,
					accessToken: accessToken,
				},
			}
		} else {
			throw new Error('Wrong credentials')
		}
	} catch (err) {
		throw err
	}
}

/**
 * Generate a new access token
 * @param {String} refreshToken
 * @returns accessToken
 */
const generateNewAccesstoken = async (refreshToken) => {
	try {
		const user = await User.findOne({ tokens: refreshToken })
		if (!user) throw new Error('Token does not exist.')
		const accessToken = generateAccessToken(user)
		return { accessToken: accessToken }
	} catch (err) {
		throw err
	}
}

/**
 * Delete a token in the database
 * @param {String} refreshToken
 * @returns {Boolean} isDeleted
 */
const deleteToken = async (refreshToken) => {
	try {
		const { nModified } = await User.updateOne(
			{ tokens: refreshToken },
			{ $pull: { tokens: refreshToken } }
		)
		if (!nModified) throw new Error('Token not find.')
		return true
	} catch (err) {
		throw err
	}
}

exports.createUser = createUser
exports.logUser = logUser
exports.generateAccessToken = generateAccessToken
exports.generateNewAccesstoken = generateNewAccesstoken
exports.deleteToken = deleteToken

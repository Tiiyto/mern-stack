const mongoose = require('mongoose')

let host = process.env.DB_HOST ? process.env.DB_HOST : 'localhost'
let port = process.env.DB_PORT ? process.env.DB_PORT : '27017'

const url = 'mongodb://' + host + ':' + port

/**
 * Connect to the database mongodb
 * @param {string} login
 * @param {string} password
 */
function connect(login, password) {
	mongoose
		.connect(url, {
			useNewUrlParser: true,
			useUnifiedTopology: true,
			useFindAndModify: false,
			useCreateIndex: true,
		})
		.then(() => console.log('successfully connected to MongoDB'))
		.catch(() => console.log('error while trying to connect to MongoDB'))
}

module.exports = connect

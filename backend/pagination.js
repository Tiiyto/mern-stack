const mongoose = require('mongoose')

/**
 *
 * @param {mongoose.Schema} schema
 * @param {number} page
 * @param {number} limit
 * @param {*} params
 * @returns
 */
const getDataPaginated = async (schema, page, limit = 5, params) => {
	if (!page || isNaN(page) || isNaN(limit)) throw new Error('Invalid Parameters')
	const start = (page - 1) * limit
	const count = await schema.countDocuments(params)
	const page_count = Math.ceil(count / limit)
	if (page > page_count || page < 1) throw new Error('Invalid request')
	const result = await schema.find(params).limit(limit).skip(start).sort({ created_at: -1 }).exec()
	const data = {
		_metadata: {
			page: page,
			per_page: limit,
			page_count: page_count,
			total_count: count,
		},
	}
	const schemaName = schema.collection.collectionName
	data[schemaName] = result
	return data
}

module.exports = getDataPaginated

const express = require('express')
const app = express()
const cookieParser = require('cookie-parser')
const connect = require('./services/connectDB')
const cors = require('cors')
const morgan = require('morgan')
const helmet = require('helmet')

const PORT = 4000

require('dotenv').config()

/** Connect to database */
if (process.env.NODE_ENV !== 'test')
	connect(process.env.MONGO_DB_USERNAME, process.env.MONGO_DB_PASSWORD)

/** Routes */
const authRoute = require('./routes/auth')
const articleRoute = require('./routes/article')
const userRoute = require('./routes/user')
const productRoute = require('./routes/product')
const orderRoute = require('./routes/order')

app.use(express.json())
app.use(
	cors({
		origin: ['http://localhost:3000', 'http://localhost:4200'],
		credentials: true,
	})
)
app.use(cookieParser())
app.use(helmet())
app.use(morgan('combined'))

app.use('/api/auth', authRoute)
app.use('/api/article', articleRoute)
app.use('/api/product', productRoute)
app.use('/api/order', orderRoute)
app.use('/api/user', userRoute)
app.use('/api/public', express.static(__dirname + '/public'))

app.get('*', (req, res) => res.sendStatus(500))

const server = app.listen(PORT, () => {
	console.log(`listening on port ${PORT}`)
})

module.exports = server

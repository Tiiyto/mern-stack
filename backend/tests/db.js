const mongoose = require('mongoose')

const connectDB = async () => {
	try {
		await mongoose.connect(`mongodb://mongo:27017`, {
			useNewUrlParser: true,
			useUnifiedTopology: true,
			useFindAndModify: false,
			useCreateIndex: true,
		})
	} catch (e) {
		console.log('Connexion à MongoDB échouée !')
	}
}

const clearDatabase = async () => {
	const collections = mongoose.connection.collections
	for (const key in collections) {
		const collection = collections[key]
		await collection.deleteMany()
	}
}

const disconnectDB = () => {
	mongoose.disconnect()
}

module.exports = { connectDB, disconnectDB, clearDatabase }

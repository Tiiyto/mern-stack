const server = require('../../server')
const supertest = require('supertest')
const { connectDB, disconnectDB, clearDatabase } = require('../db')
const Article = require('../../models/Article')
beforeAll(async () => await connectDB())
afterEach(async () => await clearDatabase())
afterAll(async () => {
	disconnectDB()
	server.close()
})

const request = supertest(server)

describe('Request article ', () => {
	it('Create', async () => {
		const article = new Article({
			title: 'title',
			description: 'description',
			content: 'content',
			author: 'author',
		})
		await article.save()

		const response = await request.get('/api/article').expect('Content-Type', /json/).expect(200)
		expect(response.body[0].title).toBe(article.title)
		expect(response.body[0].description).toBe(article.description)
		expect(response.body[0].content).toBe(article.content)
		expect(response.body[0].author).toBe(article.author)
	})
})

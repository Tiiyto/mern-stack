const { connectDB, disconnectDB, clearDatabase } = require('../db')
const bcrypt = require('bcrypt')
require('dotenv').config()
const { createUser } = require('../../services/auth.function')
const User = require('../../models/User')

beforeAll(async () => await connectDB())
afterEach(async () => await clearDatabase())
afterAll(async () => disconnectDB())

describe('User created when', () => {
	it('Basic user', async () => {
		const { _id } = await createUser(
			'Username',
			'password',
			'Firstname',
			'Lastname',
			'test@test.fr'
		)
		const user = await User.findById(_id)
		expect(user.username).toEqual('Username')
		expect(bcrypt.compareSync('password', user.password)).toEqual(true)
	})
})

describe('Errors thrown when', () => {
	it('username repeated', async () => {
		await createUser('SameUsername', 'password', 'Firstname', 'Lastname', 'test@test.fr')
		await expect(
			createUser(
				'SameUsername',
				'differentPassword',
				'differentFirstname',
				'differentLastname',
				'a@b.fr'
			)
		).rejects.toThrow()
	})
})

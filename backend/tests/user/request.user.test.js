const server = require('../../server')
const supertest = require('supertest')
const { connectDB, disconnectDB, clearDatabase } = require('../db')
const bcrypt = require('bcrypt')
const { createUser, logUser } = require('../../services/auth.function')
beforeAll(async () => await connectDB())
afterEach(async () => await clearDatabase())
afterAll(async () => {
	disconnectDB()
	server.close()
})
const request = supertest(server)

describe('Request user ', () => {
	it('Create', async () => {
		const response = await request
			.post('/api/user/register')
			.send({
				username: 'Username',
				password: 'Password',
				firstName: 'Firstname',
				lastName: 'Lastname',
				emailAddress: 'emailAddress',
			})
			.set('Accept', 'application/json')
			.expect('Content-Type', /json/)
			.expect(200)
		expect(response.body.username).toEqual('Username')
		expect(bcrypt.compareSync('Password', response.body.password)).toEqual(true)
		expect(response.body.firstName).toEqual('Firstname')
		expect(response.body.lastName).toEqual('Lastname')
		expect(response.body.emailAddress).toEqual('emailAddress')
	})
})

const express = require('express')
const { auth } = require('../middlewares/authenticateToken')
const router = express.Router()

const { getUserData, updateUser, createUser } = require('../controllers/userController')

const path = require('path')
const multer = require('multer')
var storage = multer.diskStorage({
	destination: function (req, file, cb) {
		cb(null, 'public/avatars/')
	},
	filename: function (req, file, cb) {
		cb(null, Date.now() + path.extname(file.originalname))
	},
})
const upload = multer({ storage: storage })

router.post('/register', createUser)

router.get('/data', auth, getUserData)

router.put('/edit', auth, upload.single('file'), updateUser)

module.exports = router

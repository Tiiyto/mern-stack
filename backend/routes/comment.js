const express = require('express')
const router = express.Router()
const { authAdmin } = require('../middlewares/authenticateToken')
const { getComment, createComment } = require('../controllers/commentController')

router.get('/', getComment)
router.post('/', authAdmin, createComment)

module.exports = router

const express = require("express")
const router = express.Router()

const {
  getArticle,
  createArticle,
} = require("../controllers/articleController")

router.get("/", getArticle)

router.post("/", createArticle)

module.exports = router

const express = require('express')
const router = express.Router()

const {
	cookiesLogin,
	login,
	getToken,
	cookiesLogout,
	logout,
	resetPassword,
	resetPasswordDone,
} = require('../controllers/authController')

// For mobile App
router.post('/login', login)

// For Web App
router.post('/cookies/login', cookiesLogin)

router.get('/token', getToken)

// For Web App
router.delete('/cookies/logout', cookiesLogout)

// For mobile app
router.delete('/logout', logout)

router.post('/reset-password', resetPassword)

router.post('/reset-password-done', resetPasswordDone)

module.exports = router

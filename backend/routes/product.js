const express = require('express')
const { authAdmin } = require('../middlewares/authenticateToken')
const router = express.Router()

const {
	createProduct,
	getProduct,
	getBySlug,
	getByID,
	searchProduct,
	editProduct,
	deleteProduct,
	deleteManyProduct,
} = require('../controllers/productController')

router.get('/', getProduct)

router.get('/slug/:slug', getBySlug)

router.get('/search', searchProduct)

router.get('/:id', getByID)

router.post('/', authAdmin, createProduct)

router.delete('/:id', authAdmin, deleteProduct)

router.delete('/', authAdmin, deleteManyProduct)

router.put('/:id', authAdmin, editProduct)

module.exports = router

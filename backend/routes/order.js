const express = require('express')
const router = express.Router()
const { auth, getUser } = require('../middlewares/authenticateToken')
const { getOrders, createCheckoutStripe } = require('../controllers/orderController')

router.get('/', auth, getOrders)

router.post('/charge', getUser, createCheckoutStripe)

module.exports = router

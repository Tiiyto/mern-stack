const mongoose = require('mongoose')

const orderSchema = mongoose.Schema(
	{
		receipt: {
			type: String,
			required: true,
		},
		userID: mongoose.SchemaTypes.ObjectId,
		amount: {
			type: Number,
			required: true,
		},
	},
	{ timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' } }
)

module.exports = mongoose.model('Order', orderSchema)

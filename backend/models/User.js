const mongoose = require('mongoose')

const userSchema = mongoose.Schema({
	username: {
		type: String,
		required: true,
		unique: true,
	},
	password: {
		type: String,
		required: true,
	},
	firstName: {
		type: String,
		required: true,
	},
	lastName: {
		type: String,
		required: true,
	},
	emailAddress: {
		type: String,
		required: true,
		unique: true,
	},
	isAdmin: { type: Boolean, default: false },
	tokens: { refreshToken: [String], resetPasswordToken: String },
	picture: {
		type: String,
	},
	address: { country: String, address: String, postCode: String },
	orders: {
		totalPrice: Number,
		created_at: { type: Date, default: Date.now },
		nbItems: Number,
		items: [
			{
				product: {
					title: String,

					price: Number,

					pictures: [String],
					description: String,
					link: String,
				},
				quantity: Number,
			},
		],
	},
})

module.exports = mongoose.model('User', userSchema)

const mongoose = require('mongoose')
const slugify = require('slugify')

const productSchema = mongoose.Schema(
	{
		title: {
			type: String,
			required: true,
		},
		slug: {
			type: String,
			unique: true,
			required: true,
		},
		price: {
			type: Number,
			required: true,
		},
		pictures: [String],
		description: String,
		quantity: {
			type: Number,
			required: true,
		},
	},
	{ timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' } }
)

productSchema.pre('validate', async function (next) {
	const sameSlug = await this.constructor.countDocuments({
		title: this.title,
	})
	if (sameSlug === 0) this.slug = slugify(this.title, { remove: '+' })
	else this.slug = slugify(this.title + ` ${sameSlug}`, { remove: '+' })
	next()
})

module.exports = mongoose.model('Product', productSchema)

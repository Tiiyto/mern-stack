const mongoose = require('mongoose')

const commentSchema = mongoose.Schema(
	{
		content: String,
		productID: mongoose.Types.ObjectId,
		authorID: String,
		authorTitle: String,
	},
	{ timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' } }
)

module.exports = mongoose.model('Comment', commentSchema)

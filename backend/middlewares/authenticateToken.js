const jwt = require('jsonwebtoken')
const express = require('express')

/**
 * Check the JWT token
 * @param {express.Request} req
 * @param {express.Response} res
 * @param {express.NextFunction} next
 * @returns {express.NextFunction}
 */
const auth = (req, res, next) => {
	const authHeader = req.headers.authorization
	if (authHeader.split(' ')[0] !== 'Bearer') return res.sendStatus(401)
	const token = authHeader && authHeader.split(' ')[1]
	if (token === null) return res.sendStatus(401)
	try {
		const data = jwt.verify(token, process.env.ACCESS_TOKEN_SECRET)
		req.data = data
		return next()
	} catch (err) {
		if (err) return res.sendStatus(401)
	}
}

/**
 * Check the JWT token and if the user is an admin
 * @param {express.Request} req
 * @param {express.Response} res
 * @param {express.NextFunction} next
 * @returns {express.NextFunction}
 */
const authAdmin = (req, res, next) => {
	const authHeader = req.headers.authorization
	if (!authHeader) return res.sendStatus(401)
	if (authHeader.split(' ')[0] !== 'Bearer') return res.sendStatus(401)
	const token = authHeader && authHeader.split(' ')[1]
	if (token == null) return res.sendStatus(401)
	try {
		const data = jwt.verify(token, process.env.ACCESS_TOKEN_SECRET)
		if (!data.user.isAdmin) return res.sendStatus(403)
		req.data = data
		return next()
	} catch (err) {
		if (err) return res.sendStatus(401)
	}
}

/**
 * Get the user with the JWT token
 * @param {express.Request} req
 * @param {express.Response} res
 * @param {express.NextFunction} next
 * @returns {express.NextFunction}
 */
const getUser = (req, res, next) => {
	const authHeader = req.headers.authorization
	if (authHeader.split(' ')[0] !== 'Bearer') return res.sendStatus(401)
	const token = authHeader && authHeader.split(' ')[1]
	try {
		const data = jwt.verify(token, process.env.ACCESS_TOKEN_SECRET)
		req.user = data.user
		return next()
	} catch (err) {
		return next()
	}
}

module.exports = { auth, authAdmin, getUser }

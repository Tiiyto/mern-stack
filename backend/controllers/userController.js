const User = require('../models/User')
const fs = require('fs')
const bcrypt = require('bcrypt')
const { generateAccessToken } = require('./authController')
const express = require('express')

/**
 * Create a user
 * @param {express.Request} req
 * @param {express.Response} res
 * @returns {express.Send}
 */
const createUser = async (req, res) => {
	const { username, password, firstName, lastName, emailAddress } = req.body
	if (!username || !password || !firstName || !lastName || !emailAddress) {
		return res.status(400).send({ message: 'Please give all data' })
	}
	try {
		const existingUser = await User.findOne({ username })
		if (existingUser) throw new Error('A user exists.')
		const passwordHashed = await bcrypt.hash(password, 10)
		const user = new User({
			username,
			password: passwordHashed,
			firstName,
			lastName,
			emailAddress,
		})
		await user.save()
		return res.sendStatus(200)
	} catch (error) {
		console.log(error)
		if (error.message === 'A user exists.') return res.status(409).send({ message: error.message })
		return res.sendStatus(500)
	}
}

/**
 * Get userData
 * @param {express.Request} req
 * @param {express.Response} res
 * @returns {express.Send}
 */
const getUserData = async (req, res) => {
	try {
		const user = await User.findById(req.data.user.ID)
		return res.json({
			ID: user._id,
			username: user.username,
			email: user.emailAddress,
			firstName: user.firstName,
			lastName: user.lastName,
			imageUrl: user.picture,
			address: user.address,
		})
	} catch (err) {
		console.err(err)
		return res.sendStatus(500)
	}
}

const updateUser = async (req, res) => {
	const picture = req.file
	const { firstName, lastName, emailAddress, username } = req.body
	if (!firstName && !lastName && !emailAddress && !username && !picture) {
		return res.status(400).send({ message: 'Please give some valid data' })
	}
	try {
		const user = await User.findOne({ _id: req.data.user.ID })
		if (picture) {
			if (user.picture) {
				fs.unlinkSync(`public/avatars/${user.picture}`)
			}
			user.picture = req.file.filename
		}

		if (firstName) user.firstName = firstName
		if (username) user.username = username
		if (lastName) user.lastName = lastName
		if (emailAddress) user.emailAddress = emailAddress

		await user.save()

		const token = generateAccessToken(user)
		return res.json(token)
	} catch (err) {
		console.error(err)
		return res.sendStatus(500)
	}
}

/**
 * Delete User
 * @param {express.Request} req
 * @param {express.Response} res
 * @returns {express.Send}
 */
const deleteUser = async (req, res) => {
	// TODO
}

module.exports = { createUser, getUserData, updateUser, deleteUser }

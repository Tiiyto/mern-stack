const Article = require('../models/Article')

const getArticle = (req, res) => {
	Article.find({}, (err, articles) => {
		if (err) return res.sendStatus(400)
		return res.json(articles)
	})
}

const createArticle = (req, res) => {
	const { title, description, content, author, image } = req.body
	if (!title || !description || !content || !author) {
		res.status(400).send({ message: 'Please give all data' })
	} else {
		const article = new Article({
			title: title,
			description: description,
			content: content,
			author: author,
		})
		article.save((err, article) => {
			if (err) return res.send(err)
			res.send(article)
		})
	}
}

const updateArticle = (req, res) => {
	//TODO
}

const deleteArticle = (req, res) => {
	//TODO
}

module.exports = { getArticle, createArticle }

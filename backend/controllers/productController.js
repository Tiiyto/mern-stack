const Product = require('../models/Product')
const express = require('express')
const getDataPaginated = require('../pagination')

/**
 * Get products paginated with limit and page
 * @param {express.Request} req
 * @param {express.Response} res
 * @returns {express.Send}
 */
const getProduct = async (req, res) => {
	const page = parseInt(req.query.page)
	const limit = parseInt(req.query.limit)
	try {
		const data = await getDataPaginated(Product, page, limit)
		res.send(data)
	} catch (err) {
		if (err.message === 'Invalid Parameters') return res.sendStatus(400)
		if (err.message === 'Invalid request') return res.sendStatus(404)
		console.error(err)
		return res.sendStatus(500)
	}
}

/**
 * Search a product
 * @param {express.Request} req
 * @param {express.Response} res
 * @returns {express.Send}
 */
const searchProduct = async (req, res) => {
	const { searchTerm } = req.query
	if (!searchTerm) return res.sendStatus(400)
	try {
		const regex = new RegExp(searchTerm, 'i')
		const params = { $or: [{ title: regex }, { description: regex }] }
		const data = await getDataPaginated(Product, 1, 5, params)
		if (data) return res.send(data)
		return res.sendStatus(404)
	} catch (err) {
		if (err.message === 'Invalid Parameters') return res.sendStatus(400)
		if (err.message === 'Invalid request') return res.sendStatus(404)
		console.error(err)
		return res.sendStatus(500)
	}
}

/**
 * Get product by ID
 * @param {express.Request} req
 * @param {express.Response} res
 * @returns {express.Send}
 */
const getByID = async (req, res) => {
	if (!req.params.id) return res.sendStatus(400)
	try {
		const product = await Product.findOne({ _id: req.params.id })
		if (product) return res.send(product)
		return res.sendStatus(404)
	} catch (err) {
		return res.sendStatus(500)
	}
}

/**
 * Get product by slug
 * @param {express.Request} req
 * @param {express.Response} res
 * @returns {express.Send}
 */
const getBySlug = async (req, res) => {
	if (!req.params.slug) return res.sendStatus(400)
	try {
		const product = await Product.findOne({ slug: req.params.slug })
		if (product) return res.send(product)
		return res.sendStatus(404)
	} catch (err) {
		return res.sendStatus(500)
	}
}

/**
 * Create a product
 * @param {express.Request} req
 * @param {express.Response} res
 * @returns {express.Send}
 */
const createProduct = async (req, res) => {
	const { title, price, quantity, description, picture } = req.body
	if (!title || !price || !quantity || !description || !picture) return res.sendStatus(400)
	try {
		const product = new Product({
			title,
			price,
			description,
			quantity,
			pictures: picture,
		})
		await product.save()
		res.send(product)
	} catch (err) {
		console.error(err)
		return res.sendStatus(500)
	}
}

/**
 * Delete one product with ID
 * @param {express.Request} req
 * @param {express.Response} res
 * @returns {express.Send}
 */
const deleteProduct = async (req, res) => {
	const productID = req.params.id
	if (!productID) return res.sendStatus(400)
	try {
		const product = Product.findByIdAndRemove(productID)
		if (product) return res.sendStatus(200)
		return res.sendStatus(404)
	} catch (err) {
		console.error(err)
		return res.sendStatus(500)
	}
}

/**
 * Delete many product with IDs
 * @param {express.Request} req
 * @param {express.Response} res
 * @returns {express.Send}
 */
const deleteManyProduct = async (req, res) => {
	const productIDs = req.body.ids
	if (!productIDs) return res.sendStatus(400)
	try {
		const response = await Product.remove({ _id: { $in: productIDs } })
		if (response.deletedCount !== 0) return res.sendStatus(200)
		return res.sendStatus(404)
	} catch (err) {
		console.error(err)
		return res.sendStatus(500)
	}
}

/**
 * Edit a product
 * @param {express.Request} req
 * @param {express.Response} res
 * @returns {express.Send}
 */
const editProduct = async (req, res) => {
	const { title, price, quantity, description, picture } = req.body
	const productID = req.params.id
	if (!title || !price || !quantity || !description || !picture || !productID)
		return res.sendStatus(400)
	try {
		const product = new Product({
			_id: productID,
			title,
			price,
			description,
			quantity,
			pictures: picture,
		})
		await Product.updateOne({ _id: productID }, product)
		return res.sendStatus(201)
	} catch (err) {
		console.error(err)
		return res.sendStatus(500)
	}
}

module.exports = {
	createProduct,
	getProduct,
	getBySlug,
	getByID,
	deleteProduct,
	editProduct,
	deleteManyProduct,
	searchProduct,
}

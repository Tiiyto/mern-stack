const bcrypt = require('bcrypt')
const express = require('express')
require('dotenv').config()
const nodemailer = require('nodemailer')
const jwt = require('jsonwebtoken')
const User = require('../models/User')

/**
 *	Login with Cookies
 * @param {express.Request} req
 * @param {express.Response} res
 * @returns {express.Send}
 */
const cookiesLogin = async (req, res) => {
	const { username, password } = req.body
	if (!username || !password) return res.status(400).send({ message: 'Please give all data' })
	try {
		const tokens = await logUser(username, password)
		res.cookie('refreshToken', tokens.refreshToken.toString(), {
			httpOnly: true,
			secure: process.env.NODE_ENV === 'production',
			sameSite: 'strict',
		})
		return res.json(tokens.accessToken)
	} catch (err) {
		if (err.message === 'Wrong credentials')
			return res.status(404).send({ message: 'Wrong credentials' })
		console.error(err.message)
		return res.sendStatus(500)
	}
}

/**
 *	Login
 * @param {express.Request} req
 * @param {express.Response} res
 * @returns {express.Send}
 */
const login = async (req, res) => {
	const { username, password } = req.body
	if (!username || !password) return res.status(400).send({ message: 'Please give all data' })
	try {
		const tokens = await logUser(username, password)
		return res.send(tokens)
	} catch (err) {
		if (err.message === 'Wrong credentials')
			return res.status(404).send({ message: 'Wrong credentials' })
		return res.sendStatus(500)
	}
}

/**
 *	Logout with Cookies
 * @param {express.Request} req
 * @param {express.Response} res
 * @returns {express.Send}
 */
const cookiesLogout = async (req, res) => {
	const token = req.cookies.refreshToken
	if (!token) return res.json('no token')
	try {
		const isDeleted = await deleteToken(token)
		if (isDeleted) return res.clearCookie('refreshToken').send('Log out succesfuly done')
	} catch (err) {
		return res.sendStatus(500)
	}
}

/**
 *	Logout
 * @param {express.Request} req
 * @param {express.Response} res
 * @returns {express.Send}
 */
const logout = async (req, res) => {
	const token = req.body.refreshToken
	if (!token) return res.json('no token')
	try {
		const isDeleted = await deleteToken(token)
		if (isDeleted) return res.json('token deleted')
	} catch (err) {
		return res.sendStatus(500)
	}
}

/**
 *	Generate a new access token
 * @param {express.Request} req
 * @param {express.Response} res
 * @returns {express.Send}
 */
const getToken = async (req, res) => {
	const refreshToken = req.cookies.refreshToken ? req.cookies.refreshToken : req.body.refreshToken
	if (!refreshToken) return res.status(400).send({ message: 'Please give a token.' })
	try {
		const accessToken = await generateNewAccesstoken(refreshToken)
		return res.json(accessToken)
	} catch (err) {
		return res.sendStatus(500)
	}
}

/**
 *	Send a link in an email to reset the password
 * @param {express.Request} req
 * @param {express.Response} res
 * @returns {express.Send}
 */
const resetPassword = async (req, res) => {
	const { emailAddress } = req.body
	const user = await User.findOne({ emailAddress: emailAddress }).catch((err) => {
		return res.sendStatus(500)
	})
	if (!user) return res.sendStatus(404)
	const resetPasswordToken = jwt.sign(
		{ userID: user._id },
		process.env.RESET_PASSWORD_TOKEN_SECRET,
		{ expiresIn: '10min' }
	)
	const testAccount = await nodemailer.createTestAccount()
	const transporter = nodemailer.createTransport({
		host: 'smtp.ethereal.email',
		port: 587,
		secure: false,
		auth: {
			user: testAccount.user,
			pass: testAccount.pass,
		},
	})
	const mailOptions = {
		from: testAccount.user,
		to: emailAddress,
		subject: 'Reset password',
		html: `<a href="${process.env.CLIENT_URL}/reset-password/${resetPasswordToken}"/>Reset password</a>`,
	}
	try {
		user.tokens.resetPasswordToken = resetPasswordToken
		user.save()
		const info = await transporter.sendMail(mailOptions)
		return res.send(nodemailer.getTestMessageUrl(info))
	} catch (err) {
		console.error(err)
		return res.sendStatus(500)
	}
}

/**
 *	Reset the password
 * @param {express.Request} req
 * @param {express.Response} res
 * @returns {express.Send}
 */
const resetPasswordDone = async (req, res) => {
	const { refreshPasswordToken, newPassword } = req.body
	try {
		const { userID } = jwt.verify(refreshPasswordToken, process.env.RESET_PASSWORD_TOKEN_SECRET)
		const user = await User.findById(userID)
		if (user.tokens.resetPasswordToken !== refreshPasswordToken) res.sendStatus(500)
		const passwordHashed = await bcrypt.hash(newPassword, 10)
		user.tokens.resetPasswordToken = null
		user.password = passwordHashed
		user.save()
		return res.sendStatus(200)
	} catch (err) {
		console.error(err)
		res.sendStatus(500)
	}
}

/**
 *
 * @param {User} user
 * @returns token
 */
const generateAccessToken = (user) => {
	return jwt.sign(
		{
			user: {
				ID: user._id,
				username: user.username,
				firstName: user.firstName,
				lastName: user.lastName,
				imageUrl: `${user.picture}`,
				isAdmin: user.isAdmin,
			},
		},
		process.env.ACCESS_TOKEN_SECRET,
		{ expiresIn: '10min' }
	)
}

/**
 *
 * @param {String} username
 * @param {String} password
 * @returns data
 */
const logUser = async (username, password) => {
	try {
		var regex = new RegExp(['^', username, '$'].join(''), 'i')
		const user = await User.findOne({ username: regex })
		if (!user) throw new Error('Wrong credentials')
		if (bcrypt.compareSync(password, user.password)) {
			const accessToken = generateAccessToken(user)
			const refreshToken = jwt.sign({ userID: user._id }, process.env.REFRESH_TOKEN_SECRET)
			user.tokens.refreshToken.push(refreshToken)
			await user.save()
			return {
				refreshToken: refreshToken,
				accessToken: accessToken,
			}
		} else {
			throw new Error('Wrong credentials')
		}
	} catch (err) {
		throw err
	}
}

/**
 * Generate a new access token
 * @param {String} refreshToken
 * @returns accessToken
 */
const generateNewAccesstoken = async (refreshToken) => {
	try {
		const { userID } = jwt.verify(refreshToken, process.env.REFRESH_TOKEN_SECRET)
		const user = await User.findById(userID)
		if (!user) throw new Error('Token does not exist.')
		if (!user.tokens.refreshToken.includes(refreshToken)) throw new Error('Token has been revoked.')
		const accessToken = generateAccessToken(user)
		return accessToken
	} catch (err) {
		throw err
	}
}

/**
 * Delete a token in the database
 * @param {String} refreshToken
 * @returns {Boolean} isDeleted
 */
const deleteToken = async (refreshToken) => {
	try {
		const { nModified } = await User.updateOne(
			{ 'tokens.refreshToken': refreshToken },
			{ $pull: { 'tokens.refreshToken': refreshToken } }
		)
		if (!nModified) throw new Error('Token not find.')
		return true
	} catch (err) {
		throw err
	}
}

module.exports = {
	cookiesLogin,
	login,
	getToken,
	cookiesLogout,
	logout,
	resetPassword,
	resetPasswordDone,
	generateAccessToken,
}

const Order = require('../models/Order')
const Product = require('../models/Product')
const mongoose = require('mongoose')
const stripe = require('stripe')(process.env.STRIPE_SECRET_KEY)

const getOrders = async (req, res) => {
	try {
		const id = req.data.user.ID
		const orders = await Order.find({ userID: mongoose.Types.ObjectId(id) })
		if (orders) return res.send(orders)
		return res.sendStatus(404)
	} catch (err) {
		console.error(err)
		return res.sendStatus(500)
	}
}

const createCheckoutStripe = async (req, res) => {
	const { amount, cart, source, receipt_email } = req.body
	try {
		const charge = await stripe.charges.create({
			amount: amount,
			currency: 'eur',
			source: source,
			description: 'Purchase on my web app',
			receipt_email: receipt_email,
		})
		cart.map(async (item) => {
			const product = await Product.findById(item.product._id)
			product.quantity -= item.quantity
			product.save()
		})
		let orderContent = { receipt: charge.receipt_url, amount: amount / 100 }
		if (req.user) orderContent = { ...orderContent, userID: mongoose.Types.ObjectId(req.user.ID) }
		const order = new Order(orderContent)
		order.save()
		return res.send(charge.receipt_url)
	} catch (err) {
		console.error(err)
		return res.sendStatus(500)
	}
}

module.exports = {
	getOrders,
	createCheckoutStripe,
}

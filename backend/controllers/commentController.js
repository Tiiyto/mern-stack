const Comment = require('../models/Comment')

const getComment = async (req, res) => {
	const { productID } = req.body
	try {
		const comments = await Comment.find({ productID: productID })
		return res.json(comments)
	} catch (err) {
		return res.sendStatus(400)
	}
}

const createComment = (req, res) => {
	const { id: authorID } = req.data.user
	const comment = new Comment({
		content,
		productID,
		authorID,
		authorTitle,
	})
}

const updateComment = (req, res) => {
	//TODO
}

const deleteComment = (req, res) => {
	//TODO
}

module.exports = { getComment, createComment }
